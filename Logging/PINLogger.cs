﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace Pinnuta.Logging
{
    public class PINLogger
    {
        public static Logger ApplicationLog
        {
            get;
            set;
        }

        public PINLogger()
        {
            ApplicationLog = LogManager.GetLogger("pinlogger");
            
        }

        public static void LogNow()
        {
            ApplicationLog.Info("Inside Logging");
        }

        public static void LogNow(string message)
        {
            if (ApplicationLog == null)
                ApplicationLog = LogManager.GetLogger("pinlogger");

            ApplicationLog.Debug(message);
        }
    }
}
