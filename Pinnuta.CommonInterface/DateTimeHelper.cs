﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinnuta.CommonComponents
{
    public static class DateTimeHelper
    {
        public static DateTime GetPlatformDateTime(DateTime dateTime)
        {
            return dateTime.ToUniversalTime();
        }

        public static DateTime GetLastMonthStartDateTime()
        {
            DateTime currentDate = DateTime.Now.AddMonths(-1);
            DateTime createdAfter = new DateTime(currentDate.Year, currentDate.Month, 1);
            return createdAfter.ToUniversalTime();
        }

        public static DateTime GetLastMonthEndDateTime()
        {
            DateTime currentDate = GetLastMonthStartDateTime();
            DateTime createdBefore = currentDate.AddMonths(1);
            return createdBefore.ToUniversalTime();
        }

        public static DateTime GetPreviousDayStartDateTime()
        {
            DateTime currentDate = DateTime.Now.AddDays(-1);
            DateTime createdAfter = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day);
            return createdAfter.ToUniversalTime();
        }

        public static DateTime GetPreviousDayEndDateTime()
        {
            DateTime currentDate = GetPreviousDayStartDateTime();
            DateTime createdBefore = currentDate.AddHours(24);
            return createdBefore.ToUniversalTime();
        }
    }
}
