﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pinnuta.CommonComponents.PlatformAuth
{
    public class AmazonAuthentication
    {
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string MarketID { get; set; }
        public string SellerID { get; set; }
        public string AuthToken { get; set; }
        public string ServiceURL { get; set; }
    }
}
