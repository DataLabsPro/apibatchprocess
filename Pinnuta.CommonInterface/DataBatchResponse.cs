﻿using System;
using System.Collections.Generic;
using System.Text;
using static Pinnuta.CommonComponents.CommonEnum;

namespace Pinnuta.CommonComponents
{
    public class DataBatchResponse
    {
        public ClientDataRunStatus clientDataRunStatus;
        public string CallBackToken;
        public string ErrorCode;
        public string ClientID;
    }
}
