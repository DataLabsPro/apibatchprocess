﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinnuta.CommonComponents
{
    public enum InboundProcessPeriod
    {
        PreviousDayOnly = 0,
        PreviousLast30Only,
        Previous3MonthOnly,
        Previous6MonthOnly,
        AllUp2Date
}

    public class InboundParameter
    {
        public bool PreviousDayOnly
        {
            get; set;
        }
        public bool PreviousLast30Only
        {
            get; set;
        }
        public bool Previous3MonthOnly
        {
            get; set;
        }
        public bool Previous6MonthOnly
        {
            get; set;
        }
        public bool AllUp2Date
        {
            get; set;
        }
        public bool IsFirstRun
        {
            get; set;
        }

    }
}
