﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pinnuta.CommonComponents
{
    public static class CommonEnum
    {
        public enum ClientDataRunStatus
        {
            InProgress = 0,
            Success = 1,
            Failure
        }
    }
}
