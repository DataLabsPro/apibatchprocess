﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pinnuta.InteractAPI;

namespace Pinnuta.Core
{
    public static class Authentication
    {       
        public static string AccessKey
        {
            get { return AuthenticationDetails.AccessKey; }          
        }
        public static string SecretKey
        {
            get { return AuthenticationDetails.SecretKey; }          
        }
        public static string MarketID
        {
            get { return AuthenticationDetails.MarketID; }            
        }
        public static string SellerID
        {
            get { return AuthenticationDetails.SellerID; }            
        }
        public static string AuthToken
        {
            get { return AuthenticationDetails.AuthToken; }            
        }
        public static string ServiceURL
        {
            get { return AuthenticationDetails.ServiceURL; }            
        }
    }
}
