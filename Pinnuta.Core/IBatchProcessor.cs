﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinnuta.Core
{
    public interface IBatchProcessor
    {
        Authentication GetAuthenticationDetails();

        void GetData(Authentication auth);

        //Authenticate Batch Processor Running Account

        //Open DB Connection

        //Authenticate Org (Amazon, Flipkart)

        //GetData()

        //Validate()

        //UploadData()

        //WriteLog()

        //CloseConnect()
    }
}
