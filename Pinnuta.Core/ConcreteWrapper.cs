﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinnuta.Core
{
    public class ConcreteWrapper : IBatchProcessor
    {      
        public Authentication GetAuthenticationDetails()
        {
            
            DataTable authTable = DBContext.DBOperations.GetData();
            if (authTable.Rows.Count > 0)
            {
                Authentication auth = new Authentication();
                DataRow userPlatformRow = authTable.Rows[0];

                auth.AccessKey = userPlatformRow["Platform_Accesskey"].ToString();
                auth.SecretKey = userPlatformRow["Platfrom_Secretkey"].ToString();
                auth.MarketID = userPlatformRow["Platform_Market_ID"].ToString();
                auth.SellerID = userPlatformRow["Platfrom_Seller_ID"].ToString();
                auth.AuthToken = userPlatformRow["Platform_Auth_Token"].ToString();
                auth.ServiceURL = userPlatformRow["Platform_ServiceURL"].ToString();
                return auth;
            }

            return null;
        }
        public void GetData(Authentication auth)
        {            
            if (auth == null)
                return;

            PullInfoHelper infoHelper = new PullInfoHelper(auth);
            string isMasked = InboundParametersHelper.GetSystemParameters("Masked_Data");
            bool isMaskedFlag = false;
            if (!string.IsNullOrEmpty(isMasked) && isMasked.ToUpper().Equals("True"))
                isMaskedFlag = true;
            infoHelper.PullAllLastMonthOrdersToDB();            
            //PULL all the remaining order items to db
            infoHelper.PullAllOrderItemsToDB();
           
            
        }
    }
}
