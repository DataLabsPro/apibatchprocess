﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinnuta.Core
{
    public class Authentication
    {
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string MarketID { get; set; }
        public string SellerID { get; set; }
        public string AuthToken { get; set; }
        public string ServiceURL { get; set; }
    }
}
