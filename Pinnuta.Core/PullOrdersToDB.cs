﻿using Core.Orders.Model;
using DBLayer;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using CommonComponents;
using System.Configuration;

namespace CommerceInteractAPI
{
    public class PullInfoHelper
    {
        string nextTokenFile = "Order_NextToken.txt";
        public AmazonAuthentication Authentication
        {
            get;
            set;
        }

        public PullInfoHelper(AmazonAuthentication authentication)
        {
            this.Authentication = authentication;
            nextTokenFile = ConfigurationManager.AppSettings["NextTokenFolder"].ToString() + "Order_NextToken.txt";
        }
        public void PullOrdersToDB()
        {
            //NpgsqlTransaction dbTransaction = DBLayerAct.GetNewTransaction();
            string orderNextToken = string.Empty;
            try
            {
                orderNextToken = CheckNextToken();
                DateTime dateTime = DateTime.Now;
                
                //if (dateTime.Day == 1) //first day of the month
                //    PullAllOrdersToDB(true, orderNextToken); //run full year report
                //else
                    PullAllOrdersToDB(orderNextToken);
                
                //PullAllOrdersToDB(true, orderNextToken);

                
                //dbTransaction.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine("PullInfoHelper Order Error: " + ex.Message);
            }
            finally
            {
                //dbTransaction.Connection.Close();
            }

            try
            {
                PullAllOrderItemsToDB();
            }
            catch(Exception ex)
            {
                Console.WriteLine("PullInfoHelper OrderItem Error: " + ex.Message);
            }
            finally
            {

            }
        }

        public void PullOrderItemsToDB()
        {

            try
            {
                PullAllOrderItemsToDB();
            }
            catch (Exception ex)
            {
                Console.WriteLine("PullInfoHelper OrderItem Error: " + ex.Message);
            }
            finally
            {

            }
        }

        /// <summary>
        /// pullallorderstodb
        /// </summary>
        /// <param name="overrideFullYear"></param>
        /// <param name="nextToken"></param>
        /// <param name="throttleCount"></param>
        public void PullAllOrdersToDB(string nextToken = "", int throttleCount = 0)
        {
            DBLayerAct dbLayer = new DBLayerAct();

            OrdersInboundHelper orderInbounder = new OrdersInboundHelper(this.Authentication);
            IMWSResponse nextResponse;

            if (string.IsNullOrEmpty(nextToken))
            {
                Console.WriteLine("Inside nextToken is null");
                ListOrdersResponse orderResponse = (ListOrdersResponse)orderInbounder.InvokeListOrdersALL();
                List<Order> orderList = orderResponse.ListOrdersResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderResponse;
            }
            else
            {
                Console.WriteLine("Inside nextToken next run");
                ListOrdersByNextTokenResponse orderNextResponse = orderInbounder.InvokeListOrdersByNextToken(nextToken);
                List<Order> orderList = orderNextResponse.ListOrdersByNextTokenResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderNextResponse;
            }
            string responseNextToken = GetNextToken(nextResponse);
            if (throttleCount == 2)
            {
                Console.WriteLine("ZZZZ starting.. hang on please.");
                Thread.Sleep(160000);
                Console.WriteLine("ZZZZ done.. hang on please.");
                throttleCount=0;
            }
            else
                throttleCount++;
            if (!string.IsNullOrEmpty(responseNextToken))
            {
                SaveNextToken(responseNextToken);
                PullAllOrdersToDB(responseNextToken, throttleCount);
            }
            else
            {
                SaveNextToken(string.Empty);
            }
                
            return;

        }

        //InvokeListOrdersLastMonth
        public void PullYesterdayOrdersToDB(string nextToken = "", int throttleCount = 0)
        {
            DBLayerAct dbLayer = new DBLayerAct();

            OrdersInboundHelper orderInbounder = new OrdersInboundHelper(this.Authentication);
            IMWSResponse nextResponse;

            if (string.IsNullOrEmpty(nextToken))
            {
                Console.WriteLine("Inside nextToken is null");
                ListOrdersResponse orderResponse = (ListOrdersResponse)orderInbounder.InvokeListOrders(false);
                List<Order> orderList = orderResponse.ListOrdersResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderResponse;
            }
            else
            {
                Console.WriteLine("Inside nextToken next run");
                ListOrdersByNextTokenResponse orderNextResponse = orderInbounder.InvokeListOrdersByNextToken(nextToken);
                List<Order> orderList = orderNextResponse.ListOrdersByNextTokenResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderNextResponse;
            }
            string responseNextToken = GetNextToken(nextResponse);
            if (throttleCount == 2)
            {
                Console.WriteLine("ZZZZ starting.. hang on please.");
                Thread.Sleep(160000);
                Console.WriteLine("ZZZZ done.. hang on please.");
                throttleCount = 0;
            }
            else
                throttleCount++;
            if (!string.IsNullOrEmpty(responseNextToken))
            {
                SaveNextToken(responseNextToken);
                PullAllLastMonthOrdersToDB(responseNextToken, throttleCount);
            }
            else
            {
                SaveNextToken(string.Empty);
            }

            return;

        }

        public void Pull3MonthsOrdersToDB(string nextToken = "", int throttleCount = 0)
        {
            DBLayerAct dbLayer = new DBLayerAct();

            OrdersInboundHelper orderInbounder = new OrdersInboundHelper(this.Authentication);
            IMWSResponse nextResponse;

            if (string.IsNullOrEmpty(nextToken))
            {
                Console.WriteLine("Inside nextToken is null");
                ListOrdersResponse orderResponse = (ListOrdersResponse)orderInbounder.InvokeListOrdersLast3Months();
                List<Order> orderList = orderResponse.ListOrdersResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderResponse;
            }
            else
            {
                Console.WriteLine("Inside nextToken next run");
                ListOrdersByNextTokenResponse orderNextResponse = orderInbounder.InvokeListOrdersByNextToken(nextToken);
                List<Order> orderList = orderNextResponse.ListOrdersByNextTokenResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderNextResponse;
            }
            string responseNextToken = GetNextToken(nextResponse);
            if (throttleCount == 2)
            {
                Console.WriteLine("ZZZZ starting.. hang on please.");
                Thread.Sleep(160000);
                Console.WriteLine("ZZZZ done.. hang on please.");
                throttleCount = 0;
            }
            else
                throttleCount++;
            if (!string.IsNullOrEmpty(responseNextToken))
            {
                SaveNextToken(responseNextToken);
                Pull3MonthsOrdersToDB(responseNextToken, throttleCount);
            }
            else
            {
                SaveNextToken(string.Empty);
            }

            return;

        }

        public void Pull6MonthsOrdersToDB(string nextToken = "", int throttleCount = 0)
        {
            DBLayerAct dbLayer = new DBLayerAct();

            OrdersInboundHelper orderInbounder = new OrdersInboundHelper(this.Authentication);
            IMWSResponse nextResponse;

            if (string.IsNullOrEmpty(nextToken))
            {
                Console.WriteLine("Inside nextToken is null");
                ListOrdersResponse orderResponse = (ListOrdersResponse)orderInbounder.InvokeListOrdersLast6Months();
                List<Order> orderList = orderResponse.ListOrdersResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderResponse;
            }
            else
            {
                Console.WriteLine("Inside nextToken next run");
                ListOrdersByNextTokenResponse orderNextResponse = orderInbounder.InvokeListOrdersByNextToken(nextToken);
                List<Order> orderList = orderNextResponse.ListOrdersByNextTokenResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderNextResponse;
            }
            string responseNextToken = GetNextToken(nextResponse);
            if (throttleCount == 2)
            {
                Console.WriteLine("ZZZZ starting.. hang on please.");
                Thread.Sleep(160000);
                Console.WriteLine("ZZZZ done.. hang on please.");
                throttleCount = 0;
            }
            else
                throttleCount++;
            if (!string.IsNullOrEmpty(responseNextToken))
            {
                SaveNextToken(responseNextToken);
                Pull3MonthsOrdersToDB(responseNextToken, throttleCount);
            }
            else
            {
                SaveNextToken(string.Empty);
            }

            return;

        }

        //InvokeListOrdersLastMonth
        public void PullAllLastMonthOrdersToDB(string nextToken = "", int throttleCount = 0)
        {
            DBLayerAct dbLayer = new DBLayerAct();

            OrdersInboundHelper orderInbounder = new OrdersInboundHelper(this.Authentication);
            IMWSResponse nextResponse;

            if (string.IsNullOrEmpty(nextToken))
            {
                Console.WriteLine("Inside nextToken is null");
                ListOrdersResponse orderResponse = (ListOrdersResponse)orderInbounder.InvokeListOrdersLastMonth();
                List<Order> orderList = orderResponse.ListOrdersResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderResponse;
            }
            else
            {
                Console.WriteLine("Inside nextToken next run");
                ListOrdersByNextTokenResponse orderNextResponse = orderInbounder.InvokeListOrdersByNextToken(nextToken);
                List<Order> orderList = orderNextResponse.ListOrdersByNextTokenResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderNextResponse;
            }
            string responseNextToken = GetNextToken(nextResponse);
            if (throttleCount == 2)
            {
                Console.WriteLine("ZZZZ starting.. hang on please.");
                Thread.Sleep(160000);
                Console.WriteLine("ZZZZ done.. hang on please.");
                throttleCount = 0;
            }
            else
                throttleCount++;
            if (!string.IsNullOrEmpty(responseNextToken))
            {
                SaveNextToken(responseNextToken);
                PullAllLastMonthOrdersToDB(responseNextToken, throttleCount);
            }
            else
            {
                SaveNextToken(string.Empty);
            }

            return;

        }

        public void PullAllLastMonthOrdersToDB(int day, string nextToken = "", int throttleCount = 0)
        {
            DBLayerAct dbLayer = new DBLayerAct();

            OrdersInboundHelper orderInbounder = new OrdersInboundHelper(this.Authentication);
            IMWSResponse nextResponse;

            if (string.IsNullOrEmpty(nextToken))
            {
                Console.WriteLine("Inside nextToken is null");
                ListOrdersResponse orderResponse = (ListOrdersResponse)orderInbounder.InvokeListOrdersLastMonthForDay(day);
                List<Order> orderList = orderResponse.ListOrdersResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderResponse;
            }
            else
            {
                Console.WriteLine("Inside nextToken next run");
                ListOrdersByNextTokenResponse orderNextResponse = orderInbounder.InvokeListOrdersByNextToken(nextToken);
                List<Order> orderList = orderNextResponse.ListOrdersByNextTokenResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderNextResponse;
            }
            string responseNextToken = GetNextToken(nextResponse);
            if (throttleCount == 2)
            {
                Console.WriteLine("ZZZZ starting.. hang on please.");
                Thread.Sleep(160000);
                Console.WriteLine("ZZZZ done.. hang on please.");
                throttleCount = 0;
            }
            else
                throttleCount++;
            if (!string.IsNullOrEmpty(responseNextToken))
            {
                SaveNextToken(responseNextToken);
                PullAllLastMonthOrdersToDB(responseNextToken, throttleCount);
            }
            else
            {
                SaveNextToken(string.Empty);
            }

            return;

        }

        public void PullAllCurrentMonthOrdersToDB(int day, string nextToken = "", int throttleCount = 0)
        {
            DBLayerAct dbLayer = new DBLayerAct();

            OrdersInboundHelper orderInbounder = new OrdersInboundHelper(this.Authentication);
            IMWSResponse nextResponse;

            if (string.IsNullOrEmpty(nextToken))
            {
                Console.WriteLine("Inside nextToken is null");
                ListOrdersResponse orderResponse = (ListOrdersResponse)orderInbounder.InvokeListOrdersCurrentMonth();
                List<Order> orderList = orderResponse.ListOrdersResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderResponse;
            }
            else
            {
                Console.WriteLine("Inside nextToken next run");
                ListOrdersByNextTokenResponse orderNextResponse = orderInbounder.InvokeListOrdersByNextToken(nextToken);
                List<Order> orderList = orderNextResponse.ListOrdersByNextTokenResult.Orders;
                StoreOrdersToDB(orderList);
                nextResponse = (IMWSResponse)orderNextResponse;
            }
            string responseNextToken = GetNextToken(nextResponse);
            if (throttleCount == 2)
            {
                Console.WriteLine("ZZZZ starting.. hang on please.");
                Thread.Sleep(160000);
                Console.WriteLine("ZZZZ done.. hang on please.");
                throttleCount = 0;
            }
            else
                throttleCount++;
            if (!string.IsNullOrEmpty(responseNextToken))
            {
                SaveNextToken(responseNextToken);
                PullAllCurrentMonthOrdersToDB(0, responseNextToken, throttleCount);
            }
            else
            {
                SaveNextToken(string.Empty);
            }

            return;

        }
        /// <summary>
        /// pullallorderstodb
        /// </summary>
        /// <param name="overrideFullYear"></param>
        /// <param name="nextToken"></param>
        /// <param name="throttleCount"></param>
        public void PullAllOrderItemsToDB(string nextToken = "", int throttleCount = 0)
        {
            Console.WriteLine("Inside PullAllOrderItemsToDB");
            List<string> missingAmazonIDs = GetMissingOrderItems();
            OrdersInboundHelper orderInbounder = new OrdersInboundHelper(this.Authentication);

            Console.WriteLine("Inside PullAllOrderItemsToDB count:" + missingAmazonIDs.Count);
            foreach (string msgAmazonid in missingAmazonIDs)
            {
                ListOrderItemsResponse orderItemResponse = (ListOrderItemsResponse)orderInbounder.InvokeListOrderItems(msgAmazonid);
                List<OrderItem> orderItems = orderItemResponse.ListOrderItemsResult.OrderItems;
                StoreOrderItemsToDB(orderItems, msgAmazonid);
                
                if (throttleCount == 10)
                {
                    Console.WriteLine("ZZZZ starting.. hang on please.");
                    Thread.Sleep(160000);
                    Console.WriteLine("ZZZZ done.. hang on please.");
                    throttleCount = 0;
                }
                else
                    throttleCount++;

            }
            
            return;

        }

        public List<string> GetMissingOrderItems()
        {
            DBLayerAct dbLayer = new DBLayerAct();
            List<string> missingOrderList = new List<string>();
            DataTable missingOrderIDs = dbLayer.GetMissingOrderItemIDs();
            if (missingOrderIDs != null && missingOrderIDs.Rows.Count > 0)
                missingOrderList = missingOrderIDs.AsEnumerable().Select(lt => lt.Field<string>("platformorderid")).ToList<string>();

            return missingOrderList;
        }

        public bool StoreOrdersToDB(List<Order> orderList)
        {
            DBLayerAct dbLayer = new DBLayerAct();
            //NpgsqlTransaction dbTransaction = DBLayerAct.GetNewTransaction();
            //SqlTransaction dbTransaction = DBLayerAct.GetNewTransaction();
            try
            {
                Console.WriteLine("Inside StoreOrdersToDB");
                foreach (Order order in orderList) //next list of order
                {
                    Console.WriteLine("Processing Order Number: " + order.AmazonOrderId + " Dated on : " + order.LastUpdateDate.ToShortDateString());
                    dbLayer.UpsertOrder(order);//, dbTransaction);
                }
                /*
                IList<string> platformOrderIDs = new List<string>();
                foreach(Order order in orderList)
                {
                    if (!platformOrderIDs.Contains(order.AmazonOrderId))
                        platformOrderIDs.Add(order.AmazonOrderId);
                }
                DataTable existingTable = dbLayer.GetExistingOrders(platformOrderIDs);
                foreach (Order order in orderList) //next list of order
                {
                    Console.WriteLine("Processing Order Number: " + order.AmazonOrderId + " Dated on : " + order.LastUpdateDate.ToShortDateString());
                    //dbLayer.UpsertOrder(order);//, dbTransaction);
                    dbLayer.GetPlatformOrder(order, existingTable);
                }
                int nextOrderValue = DBHelper.GetAllOrderNextID();
                foreach (DataRow orderRow in existingTable.Rows) //next list of order
                {
                    if (orderRow["orderid"] == DBNull.Value)
                        orderRow["orderid"] = nextOrderValue++;
                } */
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error in StoreOrdersToDB: " + ex.Message);
                //dbTransaction.Rollback();
                return false;
            }
            finally
            {

            }

            return true;
        }

        public bool StoreOrderItemsToDB(List<OrderItem> orderItemList, string amazonOrderID)
        {
            DBLayerAct dbLayer = new DBLayerAct();
            SqlTransaction dbTransaction = DBLayerAct.GetNewTransaction();
            try
            {
                Console.WriteLine("Inside StoreOrdersToDB");
                foreach (OrderItem orderItem in orderItemList) //next list of order
                {
                    Console.WriteLine("Processing Order Number: " + amazonOrderID + " for ASIN: " +  orderItem.ASIN);
                    dbLayer.UpsertOrderItem(orderItem, amazonOrderID, dbTransaction);
                }
                dbTransaction.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in StoreOrdersToDB: " + ex.Message);
                dbTransaction.Rollback();
                return false;
            } 
            finally
            {

            }

            return true;
        }

        public string GetNextToken(IMWSResponse response)
        {
            string responseXml = Utility.GetResponseXML(response);
            XmlDocument orderListDoc = new XmlDocument();
            orderListDoc.LoadXml(responseXml);
            if (orderListDoc == null)
                return string.Empty;
            XmlNode nextTokenNode = orderListDoc.SelectSingleNode("//NextToken");
            if (nextTokenNode != null)
            {
                if (!string.IsNullOrEmpty(nextTokenNode.InnerText))
                    return nextTokenNode.InnerText;
            }
  
            return string.Empty;
        }

        public string CheckNextToken()
        {
            string orderNextToken = string.Empty;
            FileInfo fileInfo = new FileInfo(nextTokenFile);
            if (fileInfo.Exists)
            {
                StreamReader streamReader = new StreamReader(nextTokenFile);
                orderNextToken = streamReader.ReadToEnd();
                streamReader.Close();
            }

            return orderNextToken;
        }

        public void SaveNextToken(string responseNextToken)
        {
            StreamWriter streamWriter = new StreamWriter(nextTokenFile);
            streamWriter.Write(responseNextToken);
            streamWriter.Close();
        }
    }
}
