﻿-- User: digitadmin
-- DROP USER digitadmin;

CREATE USER digitadmin WITH
  LOGIN
  SUPERUSER
  INHERIT
  CREATEDB
  CREATEROLE
  REPLICATION
  CONNECTION LIMIT 5;

GRANT pg_signal_backend, postgres TO digitadmin WITH ADMIN OPTION;

ALTER USER digitadmin SET search_path TO "$user", public, postgis;

COMMENT ON ROLE digitadmin IS 'digitup2 admin users for db 1';

-- Database: digitup2

-- DROP DATABASE digitup2;

CREATE DATABASE digitup2
    WITH 
    OWNER = digitadmin
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = 5;

COMMENT ON DATABASE digitup2
    IS 'digitup2';

ALTER DATABASE digitup2
    SET search_path TO '"$user", public, postgis';

GRANT ALL ON DATABASE digitup2 TO digitadmin;

GRANT TEMPORARY, CONNECT ON DATABASE digitup2 TO PUBLIC;

create user dbadmin;

GRANT ALL PRIVILEGES ON DATABASE DIGITUP2 TO dbadmin;
alter database digitup2 set search_path = "$user", public, postgis ;
alter role digitadmin set search_path = "$user", public, postgis ;
-- Table: public."order"

DROP TABLE public.order;

CREATE TABLE public.order
(
    order_id  SERIAL PRIMARY KEY,
    amazonorderid character varying(40) COLLATE pg_catalog."default" NOT NULL,
    sellerorderid character varying(40) COLLATE pg_catalog."default" NOT NULL,
    purchasedate timestamp without time zone,
    lastupdatedate timestamp without time zone,
    orderstatus character varying(40) COLLATE pg_catalog."default" NOT NULL,
    fulfillmentchannel character varying(40) COLLATE pg_catalog."default" NOT NULL,
    saleschannel character varying(40) COLLATE pg_catalog."default",
    shipservicelevel character varying(40) COLLATE pg_catalog."default",
    numberofitemsshipped smallint,
    numberofitemsunshipped smallint,
    paymentmethod character varying(40) COLLATE pg_catalog."default",
    marketplaceid character varying(40) COLLATE pg_catalog."default",
    shipmentservicelevelcategory character varying(40) COLLATE pg_catalog."default",
    ordertype character varying(40) COLLATE pg_catalog."default",
    earliestshipdate timestamp without time zone,
    latestshipdate timestamp without time zone,
    isbusinessorder boolean,
    isprime boolean,
    ispremiumorder boolean,
    isreplacementorder boolean,
    buyeremail character varying(40) COLLATE pg_catalog."default",
    buyername character varying(40) COLLATE pg_catalog."default",
    ordercurrency character varying(5) COLLATE pg_catalog."default",
    orderamount money,
	buyer_taxlegalname character varying(40) COLLATE pg_catalog."default",
	buyer_taxregion character varying(40) COLLATE pg_catalog."default",
	buyer_taxclassification character varying(40) COLLATE pg_catalog."default",
    shippingaddress_name character varying(60) COLLATE pg_catalog."default",
    shippingaddress_line1 character varying(60) COLLATE pg_catalog."default",
    shippingaddress_line2 character varying(60) COLLATE pg_catalog."default",
    shippingaddress_line3 character varying(40) COLLATE pg_catalog."default",
    shippingaddress_city character varying(40) COLLATE pg_catalog."default",
    shippingaddress_stateorregion character varying(40) COLLATE pg_catalog."default",
    shippingaddress_countrycode character varying(10) COLLATE pg_catalog."default",
	shippingaddress_county character varying(40) COLLATE pg_catalog."default",
	shippingaddress_district character varying(40) COLLATE pg_catalog."default",
CONSTRAINT amazonorderid_uq UNIQUE (amazonorderid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.order
    OWNER to postgres;
COMMENT ON TABLE public.order
    IS 'order table';

-- DROP TABLE public.order_item;

CREATE TABLE public.order_item
(
    order_item_id character varying(30),
    amazonorderid character varying(40),
    asin character varying(30) COLLATE pg_catalog."default" NOT NULL,
    sellersku character varying(20) COLLATE pg_catalog."default",
    orderitemid character varying(20) COLLATE pg_catalog."default",
    title character varying(200) COLLATE pg_catalog."default",
    quantityordered smallint,
    quantityshipped smallint,
    itemprice_currency character varying(10) COLLATE pg_catalog."default",
    itemprice_amount money,
    itemtax_currency character varying(10) COLLATE pg_catalog."default",
    itemtax_amount money,
    promo_currency character varying(10) COLLATE pg_catalog."default",
    promo_amount money,
    promoids character varying(200) COLLATE pg_catalog."default",
    CONSTRAINT order_item_pkey PRIMARY KEY (order_item_id),
    CONSTRAINT "order_item_Order_ID_fkey" FOREIGN KEY (amazonorderid)
        REFERENCES public.order (amazonorderid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.order_item
    OWNER to digitadmin;
COMMENT ON TABLE public.order_item
    IS 'order item table';