﻿
--drop sequence user_platfrom_seq;
CREATE SEQUENCE User_Account_Seq
 AS INTEGER
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 NO CYCLE; 

--DROP TABLE User_Platform_Authentication;
CREATE TABLE User_Account
(
	User_Account_ID INT NOT NULL PRIMARY KEY,
	Created_Date DateTime NULL, 
    Status VARCHAR(4) NOT NULL
);


CREATE SEQUENCE User_Platfrom_Auth_Seq
 AS INTEGER
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 NO CYCLE; 

--DROP TABLE User_Platform_Authentication;
CREATE TABLE User_Platform_Authentication
(
	User_Platform_Authentication_ID INT NOT NULL PRIMARY KEY,
	User_Account_id VARCHAR(100) NULL, 
    Platform_Type VARCHAR(40) NOT NULL,
    Platform_Accesskey VARCHAR(200) NULL, 
	Platfrom_Secretkey VARCHAR(200) NULL,  
	Platform_Market_ID VARCHAR(200) NULL,
	Platfrom_Seller_ID VARCHAR(200) NULL,
	Platform_Auth_Token VARCHAR(200) NULL,
	Platform_ServiceURL VARCHAR(200) NULL,
	Platform_Status VARCHAR(4) NOT NULL
);

CREATE SEQUENCE Tenant_Seq
 AS INTEGER
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 NO CYCLE; 

--DROP TABLE User_Platform_Authentication;
CREATE TABLE Tenant
(
	Tenant_ID INT NOT NULL PRIMARY KEY,
	User_Account_id INT NULL, 
    Created_date DATETIME NULL,
    Status VARCHAR(4) NOT NULL
);

--demo testing id
insert into User_Account (User_Account_ID,Created_Date,Status) 
VALUES(NEXT VALUE FOR User_Account_Seq,getdate(),'ACTV');

insert into User_Platform_Authentication (User_Platform_Authentication_ID,User_Account_id,Platform_Type,Platform_Accesskey,Platfrom_Secretkey,Platform_Market_ID,Platfrom_Seller_ID,Platform_Auth_Token,Platform_ServiceURL,Platform_Status) 
VALUES(NEXT VALUE FOR User_Platfrom_Auth_Seq,1,'AMZN-US','AKIAJHSECNQSMVQ7ZBIA','4DoTYfWtHeVNdTPLOBCXD8gzbpH6k/gAdA2qrtN7','ATVPDKIKX0DER','A2B0LQJYBA2OQJ','amzn.mws.26b06fb7-055b-845c-5eac-15e62bb00f5a','https://mws.amazonservices.com','ACTV');

insert into Tenant (Tenant_ID,User_Account_id,Created_Date,Status) 
VALUES(NEXT VALUE FOR Tenant_Seq,convert(int,(SELECT Current_Value FROM SYS.Sequences WHERE name='User_Account_Seq')),getdate(),'ACTV');
/*
user_account
user_id, authentication_id, last_login_date, 
user_account_authentication

user_platform_authentication
user_platform_id, authentication_id, platform_type, platform_accesskey, platform_secretkey, platform_market_id, platform_sellerid, platform_auth_token
*/
--demo auth
--insert into user_platform values(1,'developer','AMZN-US', 'AKIAJHSECNQSMVQ7ZBIA','4DoTYfWtHeVNdTPLOBCXD8gzbpH6k/gAdA2qrtN7','ATVPDKIKX0DER','A2B0LQJYBA2OQJ','amzn.mws.26b06fb7-055b-845c-5eac-15e62bb00f5a','ACTV');

CREATE SEQUENCE Inbound_Job_Seq
 AS INTEGER
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 NO CYCLE; 

CREATE SEQUENCE Inbound_Job_Detail_Seq
 AS INTEGER
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 NO CYCLE; 

CREATE SEQUENCE User_Job_Parameter_Seq
 AS INTEGER
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 NO CYCLE; 

CREATE SEQUENCE Inbound_Process_Temp_Seq
 AS INTEGER
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 NO CYCLE; 

CREATE TABLE Inbound_Process_Temp
(
	Inbound_Process_Temp_ID INT NOT NULL PRIMARY KEY,
	Tenant_ID INT NOT NULL,
	MarketPlace_ID INT NOT NULL,
	Date DateTime,
	Status VARCHAR(4) NOT NULL,
	Process_Type VARCHAR(4) NOT NULL,
	Follow_Up_Info VARCHAR(400) NULL,
	INFO_DATA VARCHAR2(2000) NOT NULL
);
--tenant_id one tenant can have more marketplace
--marketplace_id as amzn-us, amzn-uk, amzn-in, 
--status ACTV,REDY,PROC,DONE,INAC
--process_type is order,order_item,product,payment, other amzn mws calls
--follow-up-info basic for amzn next_token id
--info-data is ex list of order result to one string as serialized

CREATE TABLE Inbound_Job
(
	Inbound_Job_ID INT NOT NULL PRIMARY KEY,
	Platform_Type VARCHAR(40) NULL, 
    Job_Status VARCHAR(4) NOT NULL, --ACTV,INAC
);
--1,AMZN-US,ACTV
--2,AMZN-UK,INAC

--DETAIL updated by inbound batch process after each run
CREATE TABLE Inbound_Job_Detail
(
	Inbound_Job_Detail_ID INT NOT NULL PRIMARY KEY,
	Inbound_Job_ID INT,
	User_Account_ID INT,
	Last_Run_Status VARCHAR(4),
	Last_Run_Date DATETIME,
	No_Of_Processed_Rows INT,
	No_Of_New_Rows INT,
	CONSTRAINT fk_InboundJob
    FOREIGN KEY (Inbound_Job_ID)
    REFERENCES Inbound_Job (Inbound_Job_ID)
	--,useraccount id referent
);

--this table set from UI Initially to set what date to pull from date range
CREATE TABLE User_Job_Parameter
(
	User_Job_Parameter_ID INT NOT NULL PRIMARY KEY,
	User_Account_ID INT,
	Inbound_Job_ID INT,
	Previous_Day_Only BIT,
	Previous_3Months_Only BIT,
	Previous_HalfYear_Only BIT,
	All_Up2date BIT,
	Is_First_Run BIT,
	CONSTRAINT fk_InboundJobParam
    FOREIGN KEY (Inbound_Job_ID)
    REFERENCES Inbound_Job (Inbound_Job_ID)
	--,useraccount id referent
);
--1,1,1,1,1,0,0,0 run for amazon us only

--1,AMZN-US,ACTV
--2,AMZN-UK,INAC
insert into Inbound_Job(Inbound_Job_ID, Platform_Type,Job_Status) values(1,'AMZN-US','ACTV');
insert into Inbound_Job(Inbound_Job_ID, Platform_Type,Job_Status) values(2,'AMZN-Uk','INAC');
insert into user_job_parameter(User_Job_Parameter_ID, User_Account_ID, Inbound_Job_ID, Previous_Day_Only,Previous_3Months_Only, Previous_HalfYear_Only,All_Up2date,Is_First_Run) 
values(1,1,1,1,1,0,0,1);

CREATE TABLE System_Parameter
(
	System_Parameter_id INT NOT NULL PRIMARY KEY,
	Parameter_Name VARCHAR(200) NOT NULL,
	Parameter_Value VARCHAR(200) NOT NULL
);
insert into system_parameter(System_Parameter_id, Parameter_Name, Parameter_Value) 
values(1,'Masked_Data','1');
insert into system_parameter(System_Parameter_id, Parameter_Name, Parameter_Value) 
values(2,'Masked_EMail','info@tallentpro.com');

CREATE SEQUENCE Finance_Transaction_Seq
 AS INTEGER
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 NO CYCLE; 


CREATE TABLE Finance_Transaction
(
	Finance_Transaction_ID INT NOT NULL PRIMARY KEY,
	platformOrderID VARCHAR(40) NULL, 
    MERCHANTSKU VARCHAR(20) NULL,
	Transaction_Type VARCHAR(40) NULL,
	Total_Charge Money NULL,
	Promotianal_Rebate Money NULL,
	Platform_Fee Money NULL,
);

drop sequence all_order_seq;
CREATE SEQUENCE All_Order_Seq
 AS INTEGER
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 NO CYCLE; 

CREATE SEQUENCE Order_Item_Seq
 AS INTEGER
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 NO CYCLE; 


--DROP TABLE Order_Item;
--DROP TABLE All_Order;

CREATE TABLE All_Order
(
	OrderId INT NOT NULL PRIMARY KEY, 
    PlatformOrderID VARCHAR(40) NOT NULL unique,
	MerchantOrderID VARCHAR(40) NOT NULL,
	PurchaseDate DATETIME NULL,
	LastUpdateDate DATETIME NULL,
	Order_Status VARCHAR(40) NULL,
	FulfillmentChannel VARCHAR(40) NULL,
	SalesChannel VARCHAR(40) NULL,
	ShipServiceLevel VARCHAR(40) NULL,
	NumOfItemsShipped VARCHAR(40) NULL,
	NumOfItemsUnShipped VARCHAR(40) NULL,
	PaymentMethod VARCHAR(40) NULL,
	MarketPlaceID VARCHAR(40) NULL,
	ShipmentServiceLevelCategory VARCHAR(40) NULL,
	OrderType VARCHAR(40) NULL,
	EarliestShipDate DATETIME NULL,
	LatestShipDate DATETIME NULL,
	IsBusinessOrder BIT NULL,
	IsPrime BIT NULL,
	IsPremiumOrder BIT NULL,
	IsReplacementOrder BIT NULL,
	BuyerEmail VARCHAR(40) NULL,
	BuyerName VARCHAR(40) NULL,
	OrderCurrency VARCHAR(40) NULL,
	OrderAmount MONEY NOT NULL,
	Buyer_Tax_Name VARCHAR(40) NULL,
	Buyer_Tax_Region VARCHAR(40) NULL,
	Buyer_Tax_Class VARCHAR(40) NULL,
	Shipping_Address_Name VARCHAR(60) NULL,
	Shipping_Address_Line1 VARCHAR(60) NULL,
	Shipping_Address_Line2 VARCHAR(60) NULL,
	Shipping_Address_Line3 VARCHAR(60) NULL,
	Shipping_Address_City VARCHAR(40) NULL,
	Shipping_Address_StateOrRegion VARCHAR(40) NULL,
	Shipping_Address_CountryCode VARCHAR(10) NULL,
	Shipping_Address_County VARCHAR(40) NULL,
	Shipping_Address_District VARCHAR(40) NULL,
	Billing_Address_Name VARCHAR(60) NULL,
	Billing_Address_Line1 VARCHAR(60) NULL,
	Billing_Address_Line2 VARCHAR(60) NULL,
	Billing_Address_Line3 VARCHAR(60) NULL,
	Billing_Address_City VARCHAR(40) NULL,
	Billing_Address_StateOrRegion VARCHAR(40) NULL,
	Billing_Address_CountryCode VARCHAR(10) NULL,
	Billing_Address_County VARCHAR(40) NULL,
	Billing_Address_District VARCHAR(40) NULL,
	Tenant_ID INT NOT NULL,
	CONSTRAINT FK_Tenant FOREIGN KEY (Tenant_ID)     
    REFERENCES Tenant (Tenant_ID)     
);


CREATE TABLE Order_Item
(
	Order_Item_ID INT NOT NULL PRIMARY KEY,
	PlatformOrder_Item_Id VARCHAR(40) NULL, 
    PlatformOrderID VARCHAR(40) NOT NULL,
    ASIN VARCHAR(10) NULL,
	MERCHANTSKU VARCHAR(20) NULL,
	OrderItemID VARCHAR(20) NULL,
	Product_Title VARCHAR(200) NULL,
	Quantity_Ordered INT NULL,
	Quantity_Shipped INT NULL,
	ItemPrice_Currency VARCHAR(10) NULL,
	ItemPrice_Amount MONEY NOT NULL,
	ItemTax_Currency VARCHAR(10) NULL,
	ItemTax_Amount MONEY NOT NULL,
	Promo_Currency VARCHAR(10) NULL,
	Promo_Amount MONEY NOT NULL,
	PromoIds VARCHAR(200) NULL,
	CONSTRAINT fk_platformorder
    FOREIGN KEY (PlatformOrderID)
    REFERENCES All_Order (PlatformOrderID)
);


--CREATE TABLE Finance_Transaction
--(
--	Finance_Transaction_ID INT NOT NULL PRIMARY KEY,
--	platformOrderID VARCHAR(40) NULL, 
--    MERCHANTSKU VARCHAR(20) NULL,
--	Transaction_Type VARCHAR(40) NULL,
--	Total_Charge Money NULL,
--	Promotianal_Rebate Money NULL,
--	Platform_Fee Money NULL,
--);

--CREATE TABLE Finance_Transaction_Detail
--(
--	Finance_Transaction_Detail_ID INT NOT NULL PRIMARY KEY,
--	platformOrderID VARCHAR(40) NOT NULL, 
--    Fee_Type VARCHAR(20) NULL,
--	Fee_Currency_Code VARCHAR(4) NULL,
--	Fee_Amount Money NULL
--);

--date/time	
--settlement id	
--type	
--order id	
--sku	
--description	
--quantity	
--marketplace	
--fulfillment	
--order city
--order state
--order postal	
--product sales	
--shipping credits	
--gift wrap credits	
--promotional rebates	
--sales tax collected	
--Marketplace 
--Facilitator Tax	
--selling fees	
--fba fees	
--other transaction fees	
--other	
--total