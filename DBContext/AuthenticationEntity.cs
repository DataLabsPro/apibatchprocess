﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinnuta.DBContext
{
    public static class AuthenticationEntity
    {
        private static string _accessKey = string.Empty;
        private static string _secretKey = string.Empty;
        private static string _marketID = string.Empty;
        private static string _sellerID = string.Empty;
        private static string _authToken = string.Empty;
        private static string _serviceURL = string.Empty;

        public static string AccessKey
        {
            get { return _accessKey; }
            set { _accessKey = value; }
        }
        public static string SecretKey
        {
            get { return _secretKey; }
            set { _secretKey = value; }
        }
        public static string MarketID
        {
            get { return _marketID; }
            set { _marketID = value; }
        }
        public static string SellerID
        {
            get { return _sellerID; }
            set { _sellerID = value; }
        }
        public static string AuthToken
        {
            get { return _authToken; }
            set { _authToken = value; }
        }
        public static string ServiceURL
        {
            get { return _serviceURL; }
            set { _serviceURL = value; }
        }
    }
}
