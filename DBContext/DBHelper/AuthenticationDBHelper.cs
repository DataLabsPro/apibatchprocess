﻿using Pinnuta.DBContext;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBContext.DBHelper
{
    public class AuthenticationDBHelper
    {

        public static DataTable GetUserPlatformAuthenticationDetails(string tenantID = "0")
        {
            DataSet m_DataSet = new DataSet();
            var cmd = new SqlCommand();
            cmd.Connection = DBConnection.GetConnection();
            string sqlQuery = string.Format("select upa.* from user_platform_authentication upa " +
                                             "inner join tenant t on(t.user_account_id = upa.user_account_id and t.status = 'ACTV')" +
                                             "where t.Tenant_ID = {0}", tenantID);
            cmd.CommandText = sqlQuery;
            string sqlCommand = string.Empty;
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.SelectCommand = cmd;
            dataAdapter.Fill(m_DataSet);

            return m_DataSet.Tables[0];
        }

        public static DataTable GetClientList(bool includeInactive = false)
        {
            DataSet ds = new DataSet();
            var cmd = new SqlCommand();
            cmd.Connection = DBConnection.GetConnection();
            string inactiveStatus = "where Status in ('ACTV')";
            if (includeInactive)
                inactiveStatus = "where Status in ('ACTV','INAC')";
            string sqlQuery = string.Format("select * from tenant {0}", inactiveStatus);
            cmd.CommandText = sqlQuery;
            string sqlCommand = string.Empty;
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.SelectCommand = cmd;
            dataAdapter.Fill(ds);
            return ds.Tables[0];
        }

    }
}
