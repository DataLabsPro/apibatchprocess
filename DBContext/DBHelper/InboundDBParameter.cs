﻿using Pinnuta.DBContext;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBContext.DBHelper
{
    public class InboundDBParameter
    {
        //need common enum for platform type as amzn-us,amzn-uk
        public static DataTable GetInboundParameterDetails(int userAccountID = 1)
        {
            DataSet m_DataSet = new DataSet();
            var cmd = new SqlCommand();
            cmd.Connection = DBConnection.GetConnection();
            string sqlQuery = string.Format("select * from User_Job_Parameter ujp inner join Inbound_Job ij on (ij.Inbound_Job_ID = ujp.Inbound_Job_ID and ij.job_status='ACTV') where ujp.user_account_id={0}", userAccountID);
            cmd.CommandText = sqlQuery;
            string sqlCommand = string.Empty;
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.SelectCommand = cmd;
            dataAdapter.Fill(m_DataSet);

            return m_DataSet.Tables[0];
        }

        //once first run is status full then set this flag so next time it process will run only for last day/previous day
        public static bool SetInboundParameterFirstRunFlag(string userAccountID = "1")
        {
            DataSet m_DataSet = new DataSet();
            var cmd = new SqlCommand();
            cmd.Connection = DBConnection.GetConnection();
            //string sqlQuery = string.Format("update user_job_parameter set is_first_run=0 where user_job_parameter_id ="
            //                                + "(select user_job_parameter_id from User_Job_Parameter ujp"
            //                                + "inner join Inbound_Job ij on (ij.Inbound_Job_ID = ujp.Inbound_Job_ID and ij.job_status = 'ACTV')"
            //                                + "where ujp.user_account_id ={ 0})", userAccountID);
            string sqlQuery = string.Format("update user_job_parameter set is_first_run=0 where user_account_id ={0})", userAccountID);
            cmd.CommandText = sqlQuery;
            cmd.ExecuteNonQuery();
            return true;
        }

        public static DataTable GetSystemParameter(string parameter = "")
        {
            DataSet m_DataSet = new DataSet();
            var cmd = new SqlCommand();
            cmd.Connection = DBConnection.GetConnection();
            string sqlQuery;
            if (!string.IsNullOrEmpty(parameter))
                sqlQuery = string.Format("select * from System_Parameter where Parameter_Name='{0}'", parameter);
            else
                sqlQuery = string.Format("select * from System_Parameter");
            cmd.CommandText = sqlQuery;
            string sqlCommand = string.Empty;
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.SelectCommand = cmd;
            dataAdapter.Fill(m_DataSet);

            return m_DataSet.Tables[0];
        }

    }
}
