﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using Pinnuta.Logging;

namespace Pinnuta.DBContext
{
    public static class DBConnection
    {     
        static SqlConnection sqlConnection;   
        public static SqlConnection GetConnection()
        {
            if (sqlConnection== null)
            {                
                sqlConnection = new SqlConnection();                
                sqlConnection.ConnectionString = ConfigurationManager.AppSettings["DBConnection"].ToString();              
                sqlConnection.Open();
            }
            else if (sqlConnection.State == System.Data.ConnectionState.Closed)
            {
                sqlConnection = new SqlConnection();
                sqlConnection.ConnectionString = ConfigurationManager.AppSettings["DBConnection"].ToString();
                sqlConnection.Open();
            }
            return sqlConnection;
        }
        public static void CloseConnection()
        {
            if (sqlConnection != null)
            {
                sqlConnection.Close();
            }
        }
        public static void SetConnection()
        {
            using (var conn = GetConnection())
            {              

                // Insert some data
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "select * from All_Order";                  
                    var reader = cmd.ExecuteReader();
                }

                // Retrieve all rows
                using (var cmd = new SqlCommand("select * from All_Order", conn))
                {
                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                            PINLogger.LogNow(reader.GetString(0));
                }
            }

        }
        public static SqlTransaction GetNewTransaction()
        {
            var conn = GetConnection();
            SqlTransaction transaction = conn.BeginTransaction();

            return transaction;
        }

    }
}