﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.BulkOperations;
using Pinnuta.Logging;

namespace Pinnuta.DBContext
{
    public static class DBOperations
    {
        private static DataTable _recordsList;
        public static DataTable RecordsList
        {
            get { return _recordsList; }
            set { _recordsList = value; }
        }
        public static DataTable GetAuthenticationDetails(int userAccountID = 1)
        {            
            DataSet ds = new DataSet();
            var cmd = new SqlCommand();
            cmd.Connection = DBConnection.GetConnection();
            string sqlQuery = string.Format("select * from user_platform_authentication where Platform_Status='ACTV' and user_account_id={0}", userAccountID);
            cmd.CommandText = sqlQuery;
            string sqlCommand = string.Empty;
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.SelectCommand = cmd;
            dataAdapter.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {               
                //DataRow dr = ds.Tables[0].Rows[0];
                //AuthenticationEntity.AccessKey = dr["Platform_Accesskey"].ToString();
                //AuthenticationEntity.SecretKey = dr["Platfrom_Secretkey"].ToString();
                //AuthenticationEntity.MarketID = dr["Platform_Market_ID"].ToString();
                //AuthenticationEntity.SellerID = dr["Platfrom_Seller_ID"].ToString();
                //AuthenticationEntity.AuthToken = dr["Platform_Auth_Token"].ToString();
                //AuthenticationEntity.ServiceURL = dr["Platform_ServiceURL"].ToString();              
                
            }
            return ds.Tables[0];
        }

        public static string QueryBuilder()
        {
            return string.Empty;
        }
        /*
        public static void InsertData()        {
            
            if (_recordsList.Rows.Count > 0)
            {                
                using (SqlConnection con = DBConnection.GetConnection())
                {
                    //using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    using (var sqlBulkCopy = new BulkOperation(con))
                    {
                       
                        //Set the database table name
                        sqlBulkCopy.DestinationTableName = "All_Order_New";                       

                        //[OPTIONAL]: Map the DataTable columns with that of the database table
                        //Order model Property name with Database table column name mapping
                        //sqlBulkCopy.ColumnMappings.Add("MarketPlaceID", "MarketPlaceID");
                        sqlBulkCopy.ColumnMappings.Add("OrderId", "OrderId");
                        sqlBulkCopy.ColumnMappings.Add("AmazonOrderId", "PlatformOrderID");
                        sqlBulkCopy.ColumnMappings.Add("SellerOrderId", "MerchantOrderID");
                        sqlBulkCopy.ColumnMappings.Add("PurchaseDate", "PurchaseDate");
                        sqlBulkCopy.ColumnMappings.Add("LastUpdateDate", "LastUpdateDate");
                        sqlBulkCopy.ColumnMappings.Add("OrderStatus", "Order_Status");
                        sqlBulkCopy.ColumnMappings.Add("FulfillmentChannel", "FulfillmentChannel");
                        sqlBulkCopy.ColumnMappings.Add("SalesChannel", "SalesChannel");
                        sqlBulkCopy.ColumnMappings.Add("ShipServiceLevel", "ShipServiceLevel");
                        sqlBulkCopy.ColumnMappings.Add("NumberOfItemsShipped", "NumOfItemsShipped");
                        sqlBulkCopy.ColumnMappings.Add("NumberOfItemsUnshipped", "NumOfItemsUnShipped");
                        sqlBulkCopy.ColumnMappings.Add("PaymentMethod", "PaymentMethod");
                        sqlBulkCopy.ColumnMappings.Add("MarketplaceId", "MarketPlaceID");
                        sqlBulkCopy.ColumnMappings.Add("ShipmentServiceLevelCategory", "ShipmentServiceLevelCategory");
                        sqlBulkCopy.ColumnMappings.Add("OrderType", "OrderType");
                        sqlBulkCopy.ColumnMappings.Add("LatestShipDate", "LatestShipDate");
                        sqlBulkCopy.ColumnMappings.Add("IsBusinessOrder", "IsBusinessOrder");
                        sqlBulkCopy.ColumnMappings.Add("IsPremiumOrder", "IsPremiumOrder");
                        sqlBulkCopy.ColumnMappings.Add("IsPrime", "IsPrime");
                        sqlBulkCopy.ColumnMappings.Add("IsReplacementOrder", "IsReplacementOrder");
                        sqlBulkCopy.ColumnMappings.Add("BuyerEmail", "BuyerEmail");
                        sqlBulkCopy.ColumnMappings.Add("BuyerName", "BuyerName");
                        sqlBulkCopy.ColumnMappings.Add("CurrencyCode", "OrderCurrency");
                        sqlBulkCopy.ColumnMappings.Add("OrderAmount", "OrderAmount");
                        sqlBulkCopy.ColumnMappings.Add("CompanyLegalName", "Buyer_Tax_Name");
                        sqlBulkCopy.ColumnMappings.Add("TaxingRegion", "Buyer_Tax_Region");
                        sqlBulkCopy.ColumnMappings.Add("TaxClassifications", "Buyer_Tax_Class");
                        sqlBulkCopy.ColumnMappings.Add("Name", "Shipping_Address_Name");
                        sqlBulkCopy.ColumnMappings.Add("AddressLine1", "Shipping_Address_Line1");
                        sqlBulkCopy.ColumnMappings.Add("AddressLine2", "Shipping_Address_Line2");
                        sqlBulkCopy.ColumnMappings.Add("AddressLine3", "Shipping_Address_Line3");
                        sqlBulkCopy.ColumnMappings.Add("City", "Shipping_Address_City");
                        sqlBulkCopy.ColumnMappings.Add("StateOrRegion", "Shipping_Address_StateOrRegion");
                        sqlBulkCopy.ColumnMappings.Add("County", "Shipping_Address_County");
                        sqlBulkCopy.ColumnMappings.Add("District", "Shipping_Address_District");                    
                        sqlBulkCopy.ColumnMappings.Add("Tenant_ID", "Tenant_ID");

                    
                       
                        //sqlBulkCopy.WriteToServer(_recordsList);                         
                        sqlBulkCopy.BulkMerge(_recordsList);
                        DBConnection.CloseConnection();

                        //var bulk = new BulkOperation(con);
                        //bulk.DestinationTableName = "Customer";

                        //// Column Columns to Input
                        //bulk.ColumnInputExpression = c => new { c.Code, c.Name };

                        //// Choose Columns to Output
                        //bulk.ColumnOutputExpression = c => c.CustomerID;

                        //// Choose Key to Use
                        //bulk.ColumnPrimaryKeyExpression = c => c.Code;

                        //bulk.BulkMerge(customers);

                    }
                }
            }
        }*/
        public static void InsertData()
        {
            try
            {
                if (_recordsList.Rows.Count == 0)
                    return;

                PINLogger.LogNow("Inside StoreOrdersToDB");
                foreach (DataRow order in _recordsList.Rows) //next list of order
                {
                    string amazonOrderId = order[0].ToString();
                    CreateOrder(order);

                    string amazonOrderID = order["AmazonOrderId"].ToString();
                    string purchaseDate = order["PurchaseDate"].ToString();
                     PINLogger.LogNow("Processing Order Number: " + amazonOrderID + " Dated on : " + purchaseDate);
                }
            }
            catch (Exception ex)
            {
                PINLogger.LogNow(ex.Message);
            }

        }

        public static void InsertItemsData()
        {
            try
            {
                if (_recordsList.Rows.Count == 0)
                    return;

                foreach (DataRow orderItem in _recordsList.Rows) //next list of order
                {
                    CreateOrderItem(orderItem);

                    string amazonOrderID = orderItem["PlatformOrderID"].ToString();
                    string merchantSKU = orderItem["MERCHANTSKU"].ToString();
                    PINLogger.LogNow("Order Items: " + amazonOrderID + " for SKU: " + merchantSKU);
                }
            }
            catch (Exception ex)
            {
                PINLogger.LogNow(ex.Message);
            }

        }

        private static void CreateOrder(DataRow dr)
        {

            SqlCommand cmd = new SqlCommand("UpsertOrders");
            cmd.Connection = DBConnection.GetConnection();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PlatformOrderID", dr["AmazonOrderId"].ToString());
            cmd.Parameters.AddWithValue("@MerchantOrderID", dr["SellerOrderId"].ToString());
            cmd.Parameters.AddWithValue("@PurchaseDate", Convert.ToDateTime(dr["PurchaseDate"]));
            cmd.Parameters.AddWithValue("@LastUpdateDate", Convert.ToDateTime(dr["LastUpdateDate"]));
            cmd.Parameters.AddWithValue("@Order_Status ", dr["OrderStatus"].ToString());
            cmd.Parameters.AddWithValue("@FulfillmentChannel", dr["FulfillmentChannel"].ToString());
            cmd.Parameters.AddWithValue("@SalesChannel", dr["SalesChannel"].ToString());
            cmd.Parameters.AddWithValue("@ShipServiceLevel", dr["ShipServiceLevel"].ToString());
            cmd.Parameters.AddWithValue("@NumOfItemsShipped", dr["NumberOfItemsShipped"].ToString());
            cmd.Parameters.AddWithValue("@NumOfItemsUnShipped", dr["NumberOfItemsUnshipped"].ToString());
            cmd.Parameters.AddWithValue("@PaymentMethod", dr["PaymentMethod"].ToString());
            cmd.Parameters.AddWithValue("@MarketPlaceID", dr["MarketplaceId"].ToString());
            cmd.Parameters.AddWithValue("@ShipmentServiceLevelCategory", dr["ShipmentServiceLevelCategory"].ToString());
            cmd.Parameters.AddWithValue("@OrderType", dr["OrderType"].ToString());
            cmd.Parameters.AddWithValue("@EarliestShipDate", Convert.ToDateTime(dr["EarliestShipDate"]));
            cmd.Parameters.AddWithValue("@LatestShipDate", Convert.ToDateTime(dr["LatestShipDate"]));
            cmd.Parameters.AddWithValue("@IsBusinessOrder", Convert.ToBoolean(dr["IsBusinessOrder"]));
            cmd.Parameters.AddWithValue("@IsPrime", Convert.ToBoolean(dr["IsPrime"]));
            cmd.Parameters.AddWithValue("@IsPremiumOrder", Convert.ToBoolean(dr["IsPremiumOrder"]));
            cmd.Parameters.AddWithValue("@IsReplacementOrder", dr["IsReplacementOrder"].ToString());
            cmd.Parameters.AddWithValue("@BuyerEmail", dr["BuyerEmail"].ToString());
            cmd.Parameters.AddWithValue("@BuyerName", dr["BuyerName"].ToString());
            cmd.Parameters.AddWithValue("@OrderCurrency", dr["CurrencyCode"].ToString());
            cmd.Parameters.AddWithValue("@OrderAmount", Convert.ToDecimal(dr["OrderAmount"]));
            cmd.Parameters.AddWithValue("@Buyer_Tax_Name", dr["CompanyLegalName"].ToString());
            cmd.Parameters.AddWithValue("@Buyer_Tax_Region", dr["TaxingRegion"].ToString());
            cmd.Parameters.AddWithValue("@Buyer_Tax_Class", dr["TaxClassifications"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_Name", dr["Name"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_Line1", dr["AddressLine1"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_Line2", dr["AddressLine2"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_Line3", dr["AddressLine3"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_City", dr["City"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_StateOrRegion", dr["StateOrRegion"].ToString());            
            cmd.Parameters.AddWithValue("@Shipping_Address_Country", dr["Country"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_District", dr["District"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_Name", dr["Billing_Address_Name"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_Line1", dr["Billing_Address_Line1"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_Line2", dr["Billing_Address_Line2"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_Line3", dr["Billing_Address_Line3"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_City", dr["Billing_Address_City"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_StateOrRegion", dr["Billing_Address_StateOrRegion"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_CountryCode", dr["Billing_Address_CountryCode"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_Country", dr["Billing_Address_Country"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_District", dr["Billing_Address_District"].ToString());
            cmd.Parameters.AddWithValue("@Tenant_ID", Convert.ToInt32(dr["Tenant_ID"]));
            cmd.ExecuteNonQuery();

        }
        private static void UpsertOrders(DataRow dr)
        {           

            SqlCommand cmd = new SqlCommand("UpsertOrders");
            cmd.Connection = DBConnection.GetConnection();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PlatformOrderID", dr["PlatformOrderID"].ToString());
            cmd.Parameters.AddWithValue("@MerchantOrderID", dr["MerchantOrderID"].ToString());
            cmd.Parameters.AddWithValue("@PurchaseDate", Convert.ToDateTime(dr["PurchaseDate"]));
            cmd.Parameters.AddWithValue("@LastUpdateDate", Convert.ToDateTime(dr["LastUpdateDate"]));
            cmd.Parameters.AddWithValue("@Order_Status ", dr["Order_Status"].ToString());
            cmd.Parameters.AddWithValue("@FulfillmentChannel", dr["FulfillmentChannel"].ToString());
            cmd.Parameters.AddWithValue("@SalesChannel", dr["SalesChannel"].ToString());
            cmd.Parameters.AddWithValue("@ShipServiceLevel", dr["ShipServiceLevel"].ToString());
            cmd.Parameters.AddWithValue("@NumOfItemsShipped", dr["NumOfItemsShipped"].ToString());
            cmd.Parameters.AddWithValue("@NumOfItemsUnShipped", dr["NumOfItemsUnShipped"].ToString());
            cmd.Parameters.AddWithValue("@PaymentMethod", dr["PaymentMethod"].ToString());
            cmd.Parameters.AddWithValue("@MarketPlaceID", dr["MarketPlaceID"].ToString());
            cmd.Parameters.AddWithValue("@ShipmentServiceLevelCategory", dr["ShipmentServiceLevelCategory"].ToString());
            cmd.Parameters.AddWithValue("@OrderType", dr["OrderType"].ToString());
            cmd.Parameters.AddWithValue("@EarliestShipDate", Convert.ToDateTime(dr["EarliestShipDate"]));
            cmd.Parameters.AddWithValue("@LatestShipDate", Convert.ToDateTime(dr["LatestShipDate"]));
            cmd.Parameters.AddWithValue("@IsBusinessOrder", Convert.ToBoolean(dr["IsBusinessOrder"]));
            cmd.Parameters.AddWithValue("@IsPrime", Convert.ToBoolean(dr["IsPrime"]));
            cmd.Parameters.AddWithValue("@IsPremiumOrder", Convert.ToBoolean(dr["IsPremiumOrder"]));
            cmd.Parameters.AddWithValue("@IsReplacementOrder", dr["PurchaseDate"].ToString());
            cmd.Parameters.AddWithValue("@BuyerEmail", dr["BuyerEmail"].ToString());
            cmd.Parameters.AddWithValue("@BuyerName", dr["BuyerName"].ToString());
            cmd.Parameters.AddWithValue("@OrderCurrency", dr["OrderCurrency"].ToString());
            cmd.Parameters.AddWithValue("@OrderAmount",Convert.ToDecimal(dr["OrderAmount"]));
            cmd.Parameters.AddWithValue("@Buyer_Tax_Name", dr["Buyer_Tax_Name"].ToString());
            cmd.Parameters.AddWithValue("@Buyer_Tax_Region", dr["Buyer_Tax_Region"].ToString());
            cmd.Parameters.AddWithValue("@Buyer_Tax_Class", dr["Buyer_Tax_Class"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_Name", dr["Shipping_Address_Name"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_Line1", dr["Shipping_Address_Line1"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_Line2", dr["Shipping_Address_Line2"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_Line3", dr["Shipping_Address_Line3"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_City", dr["Shipping_Address_City"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_StateOrRegion", dr["Shipping_Address_StateOrRegion"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_Country", dr["Shipping_Address_Country"].ToString());
            cmd.Parameters.AddWithValue("@Shipping_Address_District", dr["Shipping_Address_District"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_Name", dr["Billing_Address_Name"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_Line1", dr["Billing_Address_Line1"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_Line2", dr["Billing_Address_Line2"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_Line3", dr["Billing_Address_Line3"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_City", dr["Billing_Address_City"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_StateOrRegion", dr["Billing_Address_StateOrRegion"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_CountryCode", dr["Billing_Address_CountryCode"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_Country", dr["Billing_Address_Country"].ToString());
            cmd.Parameters.AddWithValue("@Billing_Address_District", dr["Billing_Address_District"].ToString());
            cmd.Parameters.AddWithValue("@Tenant_ID",Convert.ToInt32(dr["Tenant_ID"]));
            cmd.ExecuteNonQuery();

        }

        private static void CreateOrderItem(DataRow dr)
        {

            SqlCommand cmd = new SqlCommand("UpsertOrderItems");
            cmd.Connection = DBConnection.GetConnection();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Order_Item_ID", dr["Order_Item_ID"].ToString());
            cmd.Parameters.AddWithValue("@PlatformOrder_Item_Id", dr["PlatformOrder_Item_Id"].ToString());
            cmd.Parameters.AddWithValue("@PlatformOrderID", dr["PlatformOrderID"].ToString());
            cmd.Parameters.AddWithValue("@ASIN", dr["ASIN"].ToString());
            cmd.Parameters.AddWithValue("@MERCHANTSKU", dr["MERCHANTSKU"].ToString());
            cmd.Parameters.AddWithValue("@OrderItemID ", dr["OrderItemID"].ToString());
            cmd.Parameters.AddWithValue("@Product_Title", dr["Product_Title"].ToString());
            cmd.Parameters.AddWithValue("@Quantity_Ordered", dr["Quantity_Ordered"].ToString());
            cmd.Parameters.AddWithValue("@Quantity_Shipped", dr["Quantity_Shipped"].ToString());
            cmd.Parameters.AddWithValue("@ItemPrice_Currency", dr["ItemPrice_Currency"].ToString());
            cmd.Parameters.AddWithValue("@ItemPrice_Amount", dr["ItemPrice_Amount"].ToString());
            cmd.Parameters.AddWithValue("@ItemTax_Currency", dr["ItemTax_Currency"].ToString());
            cmd.Parameters.AddWithValue("@ItemTax_Amount", dr["ItemTax_Amount"].ToString());
            cmd.Parameters.AddWithValue("@Promo_Currency", dr["Promo_Currency"].ToString());
            cmd.Parameters.AddWithValue("@Promo_Amount", dr["Promo_Amount"].ToString());
            cmd.Parameters.AddWithValue("@PromoIds", dr["PromoIds"].ToString());
            cmd.Parameters.AddWithValue("@Tenant_ID", dr["Tenant_ID"].ToString());
            cmd.ExecuteNonQuery();

        }

        //private static void CreateOrder(DataRow dr)
        //{
        //    using (SqlConnection con = DBConnection.GetConnection())
        //    {
        //        using (var sqlBulkCopy = new SqlBulkCopy(con))
        //        {

        //            //Set the database table name
        //            sqlBulkCopy.DestinationTableName = "PIN_Orders";

        //            //[OPTIONAL]: Map the DataTable columns with that of the database table
        //            //Order model Property name with Database table column name mapping
        //            //sqlBulkCopy.ColumnMappings.Add("MarketPlaceID", "MarketPlaceID");
        //            sqlBulkCopy.ColumnMappings.Add("OrderId", "OrderId");
        //            sqlBulkCopy.ColumnMappings.Add("AmazonOrderId", "PlatformOrderID");
        //            sqlBulkCopy.ColumnMappings.Add("SellerOrderId", "MerchantOrderID");
        //            sqlBulkCopy.ColumnMappings.Add("PurchaseDate", "PurchaseDate");
        //            sqlBulkCopy.ColumnMappings.Add("LastUpdateDate", "LastUpdateDate");
        //            sqlBulkCopy.ColumnMappings.Add("OrderStatus", "Order_Status");
        //            sqlBulkCopy.ColumnMappings.Add("FulfillmentChannel", "FulfillmentChannel");
        //            sqlBulkCopy.ColumnMappings.Add("SalesChannel", "SalesChannel");
        //            sqlBulkCopy.ColumnMappings.Add("ShipServiceLevel", "ShipServiceLevel");
        //            sqlBulkCopy.ColumnMappings.Add("NumberOfItemsShipped", "NumOfItemsShipped");
        //            sqlBulkCopy.ColumnMappings.Add("NumberOfItemsUnshipped", "NumOfItemsUnShipped");
        //            sqlBulkCopy.ColumnMappings.Add("PaymentMethod", "PaymentMethod");
        //            sqlBulkCopy.ColumnMappings.Add("MarketplaceId", "MarketPlaceID");
        //            sqlBulkCopy.ColumnMappings.Add("ShipmentServiceLevelCategory", "ShipmentServiceLevelCategory");
        //            sqlBulkCopy.ColumnMappings.Add("OrderType", "OrderType");
        //            sqlBulkCopy.ColumnMappings.Add("LatestShipDate", "LatestShipDate");
        //            sqlBulkCopy.ColumnMappings.Add("IsBusinessOrder", "IsBusinessOrder");
        //            sqlBulkCopy.ColumnMappings.Add("IsPremiumOrder", "IsPremiumOrder");
        //            sqlBulkCopy.ColumnMappings.Add("IsPrime", "IsPrime");
        //            sqlBulkCopy.ColumnMappings.Add("IsReplacementOrder", "IsReplacementOrder");
        //            sqlBulkCopy.ColumnMappings.Add("BuyerEmail", "BuyerEmail");
        //            sqlBulkCopy.ColumnMappings.Add("BuyerName", "BuyerName");
        //            sqlBulkCopy.ColumnMappings.Add("CurrencyCode", "OrderCurrency");
        //            sqlBulkCopy.ColumnMappings.Add("OrderAmount", "OrderAmount");
        //            sqlBulkCopy.ColumnMappings.Add("CompanyLegalName", "Buyer_Tax_Name");
        //            sqlBulkCopy.ColumnMappings.Add("TaxingRegion", "Buyer_Tax_Region");
        //            sqlBulkCopy.ColumnMappings.Add("TaxClassifications", "Buyer_Tax_Class");
        //            sqlBulkCopy.ColumnMappings.Add("Name", "Shipping_Address_Name");
        //            sqlBulkCopy.ColumnMappings.Add("AddressLine1", "Shipping_Address_Line1");
        //            sqlBulkCopy.ColumnMappings.Add("AddressLine2", "Shipping_Address_Line2");
        //            sqlBulkCopy.ColumnMappings.Add("AddressLine3", "Shipping_Address_Line3");
        //            sqlBulkCopy.ColumnMappings.Add("City", "Shipping_Address_City");
        //            sqlBulkCopy.ColumnMappings.Add("StateOrRegion", "Shipping_Address_StateOrRegion");
        //            sqlBulkCopy.ColumnMappings.Add("County", "Shipping_Address_County");
        //            sqlBulkCopy.ColumnMappings.Add("District", "Shipping_Address_District");
        //            sqlBulkCopy.ColumnMappings.Add("Tenant_ID", "Tenant_ID");

        //            sqlBulkCopy.WriteToServer(_recordsList); 
        //            DBConnection.CloseConnection();

        //        }
        //    }

        //}



        public static DataTable GetMissingOrderItemIDs()
        {
            DataTable missingOrderItemsIDs = null;

            using (var cmd = new SqlCommand())
            {
                cmd.Connection = DBConnection.GetConnection();
                DataSet m_DataSet = new DataSet();
                cmd.CommandText = string.Format("select platformorderid from PIN_Orders where platformorderid not in (select platformorderid from PIN_OrderItems)");
                string sqlCommand = string.Empty;
                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(m_DataSet, "PIN_Orders");

                missingOrderItemsIDs = m_DataSet.Tables["PIN_Orders"];
            }
            return missingOrderItemsIDs;
        }
        private static DataTable GetAmazonOrderExist(string amazonOrderNumber)
        {
            DataTable getOrder = null;

            using (var cmd = new SqlCommand())
            {
                cmd.Connection = DBConnection.GetConnection();
                DataSet m_DataSet = new DataSet();
                cmd.CommandText = string.Format("select * from PIN_Orders where platformorderid='{0}'", amazonOrderNumber);
                string sqlCommand = string.Empty;
                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(m_DataSet, "PIN_Order_Row");

                getOrder = m_DataSet.Tables["PIN_Order_Row"];
            }
            return getOrder;

        }
        public static int GetLastRecordNumber(string strTable, string strfieldName)
        {
            int recordNumber = 0;
            DataSet ds = new DataSet();
            var cmd = new SqlCommand();           
            cmd.Connection= DBConnection.GetConnection();            
            string sqlQuery = string.Format("select max({0}) from {1}", strfieldName, strTable);
            cmd.CommandText = sqlQuery;
            object result =cmd.ExecuteScalar();
            if (result.GetType() != typeof(DBNull))
            {
                recordNumber = Convert.ToInt32(result);                
            }
            return recordNumber;
        }


        //private static bool IsAmazonOrderExist(string amazonOrderNumber)
        //{           
        //    DataSet ds = new DataSet();
        //    var cmd = new SqlCommand();
        //    cmd.Connection = DBConnection.GetConnection();
        //    string sqlQuery = string.Format("select * from PIN_Orders where platformorderid='{0}'", amazonOrderNumber);
        //    cmd.CommandText = sqlQuery;
        //    return Convert.ToBoolean(cmd.ExecuteScalar()); 

        //    // PINLogger.LogNow("Processing Order Number: " + order.AmazonOrderId + " Dated on : " + order.LastUpdateDate.ToShortDateString());

        //}

        //public static void GetData()
        //{
        //    PullInfoHelper infoHelper = new PullInfoHelper(auth);
        //    infoHelper.PullAllLastMonthOrdersToDB();

        //}
        //public bool UpsertOrder(Order order, SqlTransaction transaction = null)
        //{
        //    SqlTransaction lcTransaction = DBLayerAct.GetNewTransaction();

        //    string amazonOrderId = order.AmazonOrderId;
        //    if (string.IsNullOrEmpty(amazonOrderId))
        //        return false;
        //    try
        //    {
        //        // using ()
        //        // {


        //        DataSet m_DataSet = new DataSet();
        //        var cmd = new SqlCommand();
        //        cmd.Connection = DBLayerAct.GetConnection();
        //        cmd.Transaction = lcTransaction;
        //        cmd.CommandText = string.Format("select * from {0} where platformorderid='{1}'", DBHelper.GetOrderTableName(), amazonOrderId);
        //        string sqlCommand = string.Empty;
        //        SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
        //        dataAdapter.SelectCommand = cmd;
        //        dataAdapter.Fill(m_DataSet, "all_order");

        //        DataTable orderTable = m_DataSet.Tables["all_order"];
        //        // DataRow orderRow;
        //        sqlCommand = string.Empty;
        //        if (orderTable != null && orderTable.Rows.Count > 0)
        //        {
        //            sqlCommand = DBHelper.UpdateOrderRowSQLCommand(order);
        //            cmd.CommandText = sqlCommand;
        //            cmd.Transaction = transaction;
        //            int exeResult = cmd.ExecuteNonQuery();
        //            // transaction.Commit();
        //        }
        //        else
        //        {
        //            //orderRow = orderTable.NewRow();
        //            var insertCmd = new SqlCommand();
        //            insertCmd.Connection = DBLayerAct.GetConnection();
        //            insertCmd.Transaction = lcTransaction;
        //            sqlCommand = DBHelper.CreateOrderRowSQLCommand(order);//, orderRow);
        //            insertCmd.CommandText = sqlCommand;

        //            //cmd.Connection = conn;
        //            //cmd.Transaction = transaction;
        //            int exeResult = insertCmd.ExecuteNonQuery();
        //            lcTransaction.Commit();
        //        }


        //        // int response = dataAdapter.Update(m_DataSet);
        //        //conn.Close();

        //        return true;
        //        // }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorstring = ex.Message;
        //        errorstring += "sdf";
        //        lcTransaction.Rollback();
        //    }
        //    finally
        //    {
        //        lcTransaction.Dispose();
        //    }

        //    return false;
        //}

        ////public bool UpsertOrderItem(OrderItem orderItem, string amazonOrderId, SqlTransaction transaction)
        //{
        //    string asinID = orderItem.ASIN;
        //    if (string.IsNullOrEmpty(asinID))
        //        return false;
        //    try
        //    {
        //        // using ()
        //        // {

        //        //NpgsqlTransaction transaction = conn.BeginTransaction();

        //        DataSet m_DataSet = new DataSet();
        //        var cmd = new SqlCommand();
        //        cmd.Connection = DBLayerAct.GetConnection();
        //        cmd.Transaction = transaction;
        //        cmd.CommandText = string.Format("select * from {0} where platformorderid='{1}' and asin='{2}'", DBHelper.GetOrderItemTableName(), amazonOrderId, asinID);
        //        string sqlCommand = string.Empty;
        //        SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
        //        dataAdapter.SelectCommand = cmd;
        //        dataAdapter.Fill(m_DataSet, "order_item");

        //        DataTable orderTable = m_DataSet.Tables["order_item"];
        //        //DataRow orderRow;
        //        sqlCommand = string.Empty;
        //        if (orderTable != null && orderTable.Rows.Count > 0)
        //        {
        //            sqlCommand = DBHelper.UpdateOrderItemRowSQLCommand(orderItem, amazonOrderId);
        //            cmd.CommandText = sqlCommand;
        //            cmd.Transaction = transaction;
        //            int exeResult = cmd.ExecuteNonQuery();
        //            //transaction.Commit();
        //        }
        //        else
        //        {
        //            //orderRow = orderTable.NewRow();
        //            sqlCommand = DBHelper.CreateOrderItemRowSQLCommand(orderItem, amazonOrderId);//, orderRow);
        //            cmd.CommandText = sqlCommand;
        //            //cmd.Connection = conn;
        //            cmd.Transaction = transaction;
        //            int exeResult = cmd.ExecuteNonQuery();
        //            //transaction.Commit();
        //        }


        //        // int response = dataAdapter.Update(m_DataSet);
        //        //conn.Close();

        //        return true;
        //        // }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorstring = ex.Message;
        //        errorstring += "sdf";
        //    }

        //    return false;
        //}


    }
}
