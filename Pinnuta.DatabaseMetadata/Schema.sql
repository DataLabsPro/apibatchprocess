﻿CREATE SCHEMA [Schema]


ALTER TABLE [dbo].[User_Job_Parameter] DROP CONSTRAINT [fk_InboundJobParam]
GO

/****** Object:  Table [dbo].[User_Job_Parameter]    Script Date: 2/18/2019 12:59:08 PM ******/
DROP TABLE [dbo].[User_Job_Parameter]
GO

/****** Object:  Table [dbo].[User_Job_Parameter]    Script Date: 2/18/2019 12:59:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[User_Job_Parameter](
	[User_Job_Parameter_ID] [int] NOT NULL,
	[User_Account_ID] [int] NULL,
	[Inbound_Job_ID] [int] NULL,
	[Previous_Day_Only] [bit] NULL,
	[Previous_Last30_Only] [bit] NULL,
	[Previous_3Months_Only] [bit] NULL,
	[Previous_HalfYear_Only] [bit] NULL,
	[All_Up2date] [bit] NULL,
	[Is_First_Run] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[User_Job_Parameter_ID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[User_Job_Parameter]  WITH CHECK ADD  CONSTRAINT [fk_InboundJobParam] FOREIGN KEY([Inbound_Job_ID])
REFERENCES [dbo].[Inbound_Job] ([Inbound_Job_ID])
GO

ALTER TABLE [dbo].[User_Job_Parameter] CHECK CONSTRAINT [fk_InboundJobParam]
GO


