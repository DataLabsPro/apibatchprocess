﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pinnuta.DBContext;

namespace Pinnuta.InteractAPI
{
    public static class AuthenticationDetails    {    

        public static string AccessKey
        {
            get { return AuthenticationEntity.AccessKey; }          
        }
        public static string SecretKey
        {
            get { return AuthenticationEntity.SecretKey; }          
        }
        public static string MarketID
        {
            get { return AuthenticationEntity.MarketID; }            
        }
        public static string SellerID
        {
            get { return AuthenticationEntity.SellerID; }            
        }
        public static string AuthToken
        {
            get { return AuthenticationEntity.SellerID; }            
        }
        public static string ServiceURL
        {
            get { return AuthenticationEntity.ServiceURL; }            
        }
    }
}
