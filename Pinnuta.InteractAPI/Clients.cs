﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Data;
using DBContext.DBHelper; 

namespace Pinnuta.InteractAPI
{
    public class Client
    {
        
        public string TenantID
        {
            get;set;
        }

        public string UserAccountID
        {
            get; set;
        }

        public string UserDisplayName
        {
            get; set;
        }
    }

    public static class ClientHelper
    {
        public static IDictionary<string, Client> GetClientList(bool includeInactive = false)
        {
            IDictionary<string, Client> clientList = new Dictionary<string, Client>();

            DataTable clientTable = AuthenticationDBHelper.GetClientList();
            foreach(DataRow clientRow in clientTable.Rows)
            {
                string tenantID = clientRow["tenant_id"].ToString();

                Client clientInfo = new Client();
                clientInfo.TenantID = tenantID;
                clientInfo.UserAccountID = "1";
                clientInfo.UserDisplayName = "Demo";

                clientList.Add(tenantID, clientInfo);
            }

            return clientList;
        }
    }
}
