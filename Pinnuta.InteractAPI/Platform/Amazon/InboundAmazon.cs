﻿using Pinnuta.CommonComponents;
using Pinnuta.CommonComponents.PlatformAuth;
using Pinnuta.InteractAPI.AuthenticationHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Pinnuta.CommonComponents.CommonEnum;

namespace Pinnuta.InteractAPI.Platform
{
    public class InboundAmazon : IDataRequestProcess
    {
        public AmazonAuthentication Authentication
        {
            get;
            set;
        }
        public Client Tenant
        {
            get;set;
        }

        public InboundParameter InboundParameterData { get; set; }

        public DataBatchResponse dataBatchResponse = null;

        public InboundProcessPeriod dataProcessPeriod { get; set; }

        public InboundAmazon(Client client, string callbackNextToken = "")
        {
            Tenant = client;
            Authentication = AuthenticationHelper.AuthenticationHelper.GetAmazonAuthentication(Tenant.TenantID);
            InboundParameterData = InboundParametersHelper.GetInbountParameters();
            dataProcessPeriod = InboundParametersHelper.GetUserInboundRunPeriod(InboundParameterData);
            dataBatchResponse = new DataBatchResponse();
            dataBatchResponse.ClientID = Tenant.TenantID;
            dataBatchResponse.CallBackToken = callbackNextToken;
            //Tenant = AuthenticationHelper.
        }

        public DataBatchResponse ExecutePlatformInboundData()
        {
            dataBatchResponse.clientDataRunStatus = ClientDataRunStatus.Failure;
            try
            {
                if (Authentication == null)
                {
                    dataBatchResponse.clientDataRunStatus = ClientDataRunStatus.Failure;
                    dataBatchResponse.ErrorCode = "Authentication Error";
                }
                
               // IPlatformDataRequester platformDataRequester = new PullInfoHelper(Authentication);
                //platformDataRequester.PullAllLastMonthData();

                PullInfoHelper infoHelper = new PullInfoHelper(Authentication);
                string isMasked = InboundParametersHelper.GetSystemParameters("Masked_Data");
                bool isMaskedFlag = false;
                //if (!string.IsNullOrEmpty(isMasked) && isMasked.ToUpper().Equals("True"))
                //    isMaskedFlag = true;
                infoHelper.PullPinOrdersToDB(dataProcessPeriod, this.dataBatchResponse.CallBackToken);
                //PULL all the remaining order items to db
                bool pullItemStatus = infoHelper.PullAllOrderItemsToDB();
                if (!pullItemStatus)
                {
                    dataBatchResponse.clientDataRunStatus = ClientDataRunStatus.Failure;
                    dataBatchResponse.ErrorCode = "Data SubTables did not run";
                }
                else
                {
                    //InboundParametersHelper.SetInbountParametersIsFirstFlag();
                    if (infoHelper.CallBackNextToken.Count != 0)
                        dataBatchResponse.CallBackToken = infoHelper.CallBackNextToken.Pop();
                    else
                        dataBatchResponse.CallBackToken = string.Empty;
                    if (string.IsNullOrEmpty(infoHelper.CheckNextToken()))
                    {
                        dataBatchResponse.clientDataRunStatus = ClientDataRunStatus.Success;
                        if (InboundParametersHelper.IsInboundParameterFirstRun(InboundParameterData))
                            InboundParametersHelper.SetInbountParametersIsFirstFlag(this.Tenant.UserAccountID);
                    }
                    else
                        dataBatchResponse.clientDataRunStatus = ClientDataRunStatus.InProgress;
                }
            }
            catch(Exception ex)
            {
                dataBatchResponse.clientDataRunStatus = ClientDataRunStatus.Failure;
                dataBatchResponse.ErrorCode = ex.Message;
            }
            // PullFinanceInfoHelper financeHelper = new PullFinanceInfoHelper(Authentication);
            //  financeHelper.PullFinanceDataToDB();

            return dataBatchResponse;
        }
    }
}
