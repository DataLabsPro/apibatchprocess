﻿using DBContext.DBHelper;
using Pinnuta.CommonComponents;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinnuta.InteractAPI.Platform
{
    public class InboundParametersHelper
    {
        /// <summary>
        /// right now default to amazon -us but need changes to other platform
        /// if is_first_run is active then run max order period to pull for any user, if not first run then run only last day/yesterday orders.
        /// </summary>
        /// <returns></returns>
        public static InboundParameter GetInbountParameters()
        {
            DataTable parameterTable = InboundDBParameter.GetInboundParameterDetails();
            if (parameterTable.Rows.Count > 0)
            {
                InboundParameter inboundParameter = new InboundParameter();
                DataRow userPlatformRow = parameterTable.Rows[0];

                inboundParameter.IsFirstRun = Convert.ToBoolean(userPlatformRow["Is_First_Run"]);
                inboundParameter.PreviousDayOnly = Convert.ToBoolean(userPlatformRow["Previous_Day_Only"]);
                inboundParameter.PreviousLast30Only = Convert.ToBoolean(userPlatformRow["Previous_Last30_Only"]);
                inboundParameter.Previous3MonthOnly = Convert.ToBoolean(userPlatformRow["Previous_3Months_Only"]);
                inboundParameter.Previous6MonthOnly = Convert.ToBoolean(userPlatformRow["Previous_HalfYear_Only"]);
                inboundParameter.AllUp2Date = Convert.ToBoolean(userPlatformRow["All_Up2date"]);

                return inboundParameter;
            }

            return null;
        }

        public static bool IsInboundParameterFirstRun(InboundParameter inboundParameter)
        {
            if (inboundParameter.IsFirstRun)
                return true;

            return false;
        }
        public static InboundProcessPeriod GetUserInboundRunPeriod(InboundParameter inboundParameter)
        {
            // by default is only last day/previous day 
            InboundProcessPeriod inboundProcessPeriod = InboundProcessPeriod.PreviousDayOnly;

            if (inboundParameter.IsFirstRun)
            {
                if (inboundParameter.AllUp2Date)
                    inboundProcessPeriod = InboundProcessPeriod.AllUp2Date;
                else if (inboundParameter.Previous6MonthOnly)
                    inboundProcessPeriod = InboundProcessPeriod.Previous6MonthOnly;
                else if (inboundParameter.Previous3MonthOnly)
                    inboundProcessPeriod = InboundProcessPeriod.Previous3MonthOnly;
                else if (inboundParameter.PreviousLast30Only)
                    inboundProcessPeriod = InboundProcessPeriod.PreviousLast30Only;
                else if (inboundParameter.PreviousDayOnly)
                    inboundProcessPeriod = InboundProcessPeriod.PreviousDayOnly;
            }
            return inboundProcessPeriod;
        }

        public static bool SetInbountParametersIsFirstFlag(string userAccountID)
        {
            return InboundDBParameter.SetInboundParameterFirstRunFlag(userAccountID);
        }

        public static Dictionary<String, String> GeSystemParameters()
        {
            DataTable parameterTable = InboundDBParameter.GetSystemParameter();
            if (parameterTable.Rows.Count > 0)
            {
                Dictionary<String, String> systemParameter = new Dictionary<string, string>();
                foreach (DataRow parameterRow in parameterTable.Rows)
                {
                    string parameterName = parameterRow["Parameter_Name"].ToString();
                    string parameterValue = parameterRow["Parameter_Value"].ToString();
                    if (!systemParameter.ContainsKey(parameterName))
                        systemParameter.Add(parameterName, parameterValue);
                }
                return systemParameter;
            }

            return null;
        }

        public static string GetSystemParameters(string parameterName)
        {
            DataTable parameterTable = InboundDBParameter.GetSystemParameter(parameterName);
            if (parameterTable.Rows.Count > 0)
            {
                string parameterValue = parameterTable.Rows[0]["Parameter_Value"].ToString();
                return parameterValue;
            }

            return string.Empty;
        }
    }
}
