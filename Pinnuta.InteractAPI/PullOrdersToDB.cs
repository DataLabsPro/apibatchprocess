﻿using Pinnuta.PlatformCore.Model;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Configuration;
using Pinnuta.DBContext;
using Pinnuta.CommonComponents.PlatformAuth;
using Pinnuta.Logging;
using Pinnuta.CommonComponents;

namespace Pinnuta.InteractAPI
{
    public class PullInfoHelper
    {
        string nextTokenFile = "Order_NextToken.txt";
        public AmazonAuthentication Authentication
        {
            get;
            set;
        }

        public List<string> OrderLists
        {
            get;
            set;
        }

        public Stack<string> CallBackNextToken
        {
            get;
            set;
        }

        public PullInfoHelper(AmazonAuthentication authentication)
        {
            this.Authentication = authentication;
            //nextTokenFile = ConfigurationManager.AppSettings["NextTokenFolder"].ToString() + "Order_NextToken.txt";
            CallBackNextToken = new Stack<string>();
        }

        /// <summary>
        /// pullallorderstodb
        /// </summary>
        /// <param name="overrideFullYear"></param>
        /// <param name="nextToken"></param>
        /// <param name="throttleCount"></param>

        //InvokeListOrdersLastMonth
        public void PullPinOrdersToDB(InboundProcessPeriod processPeriod, string nextToken = "", int throttleCount = 0)
        {
            IMWSResponse nextResponse;
            this.OrderLists = null;

            if (string.IsNullOrEmpty(nextToken))
                CheckNextToken();

            List<Order> orderList = new List<Order>();
            ListOrdersResponse orderResponse = null;
            OrdersInboundHelper orderInbounder = new OrdersInboundHelper(this.Authentication);
            
            if (string.IsNullOrEmpty(nextToken))
            {
                orderResponse = (ListOrdersResponse)orderInbounder.InvokeListOrders(processPeriod);
                orderList = orderResponse.ListOrdersResult.Orders;
                nextResponse = (IMWSResponse)orderResponse;
            }
            else
            {
                ListOrdersByNextTokenResponse orderNextResponse = orderInbounder.InvokeListOrdersByNextToken(nextToken);
                orderList = orderNextResponse.ListOrdersByNextTokenResult.Orders;
                nextResponse = (IMWSResponse)orderNextResponse;
            }
            
            SetOrdersList(orderList);
            StoreOrdersToDB(orderList);

            string responseNextToken = GetNextToken(nextResponse);
            if (!string.IsNullOrEmpty(responseNextToken))
            {
                //SaveNextToken(responseNextToken);
                CallBackNextToken.Push(responseNextToken);
            }
            return;
            if (throttleCount == 2)
            {
                PINLogger.LogNow("ZZZZ starting.. hang on please.");
                Thread.Sleep(160000);
                PINLogger.LogNow("ZZZZ done.. hang on please.");
                throttleCount = 0;
            }
            else
                throttleCount++;
            

            return;

        }
        /// <summary>
        /// pullallorderstodb
        /// </summary>
        /// <param name="overrideFullYear"></param>
        /// <param name="nextToken"></param>
        /// <param name="throttleCount"></param>
        public bool PullAllOrderItemsToDB(string nextToken = "", int throttleCount = 0)
        {
            PINLogger.LogNow("Inside PullAllOrderItemsToDB");
            PINLogger.LogNow();
            List<string> missingAmazonIDs = new List<string>();
            if (this.OrderLists == null)
            {
                missingAmazonIDs = GetMissingOrderItems();
            }
            else
                missingAmazonIDs = this.OrderLists;

            OrdersInboundHelper orderInbounder = new OrdersInboundHelper(this.Authentication);

            PINLogger.LogNow("Inside PullAllOrderItemsToDB count:" + missingAmazonIDs.Count);
            foreach (string msgAmazonid in missingAmazonIDs)
            {
                ListOrderItemsResponse orderItemResponse = null;
                try
                {
                    orderItemResponse = (ListOrderItemsResponse)orderInbounder.InvokeListOrderItems(msgAmazonid);
                    List<OrderItem> orderItems = orderItemResponse.ListOrderItemsResult.OrderItems;
                    StoreOrderItemsToDB(orderItems, msgAmazonid);
                    string itemNextToken = orderItemResponse.ListOrderItemsResult.NextToken;
                    ResponseHeaderMetadata headerMetadata = orderItemResponse.ResponseHeaderMetadata;
                    //CallBackNextToken.Push(itemNextToken);

                    //if (throttleCount == 10)
                    //{
                    //    PINLogger.LogNow("ZZZZ starting.. hang on please.");
                    //    Thread.Sleep(160000);
                    //    PINLogger.LogNow("ZZZZ done.. hang on please.");
                    //    throttleCount = 0;
                    //}
                    //else
                    //    throttleCount++;
                }
                catch (SqlException ex)
                {
                    //need to retry so return with error
                    return false;
                    //order id does not exist. need to log to investigate for back office
                }
            }
            //string nextOrderToken = CallBackNextToken.Pop();
            //SaveNextToken(nextOrderToken);
            return true;

        }

        public void SetOrdersResponseList(ListOrdersResponse listOrdersResponse)
        {
            List<Order> orderList = listOrdersResponse.ListOrdersResult.Orders;
            SetOrdersList(orderList);
        }

        public void GetOrdersNextResponseList(ListOrdersByNextTokenResponse listOrdersNextResponse)
        {
            List<Order> orderList = listOrdersNextResponse.ListOrdersByNextTokenResult.Orders;
            SetOrdersList(orderList);
        }

        public void SetOrdersList(List<Order> listOrders)
        {
            List<string> orderPlatformIDs = new List<string>();
            listOrders.ForEach(order => { orderPlatformIDs.Add(order.AmazonOrderId); });

            OrderLists = orderPlatformIDs;
        }

        public List<string> GetMissingOrderItems()
        {           
            List<string> missingOrderList = new List<string>();
            DataTable missingOrderIDs = DBOperations.GetMissingOrderItemIDs();
            if (missingOrderIDs != null && missingOrderIDs.Rows.Count > 0)
                missingOrderList = missingOrderIDs.AsEnumerable().Select(lt => lt.Field<string>("platformorderid")).ToList<string>();

            return missingOrderList;
        }

        public bool StoreOrdersToDB(List<Order> orderList)
        {
            //SqlTransaction dbTransaction = DBConnection.GetNewTransaction();
            try
            {
               
                DataTable dt = ConvertListToDataTable(orderList);               
                DBConnector.RecordsList = dt;              
                DBConnector.InsertData();               
                foreach (Order order in orderList) //next list of order
                {
                    PINLogger.LogNow("Processed Order Number: " + order.AmazonOrderId + " Dated on : " + order.LastUpdateDate.ToShortDateString());
                    //dbLayer.UpsertOrder(order);//, dbTransaction);
                }
                //dbTransaction.Commit();
            }
            catch(Exception ex)
            {
                PINLogger.LogNow("Error in StoreOrdersToDB: " + ex.Message);
               // dbTransaction.Rollback();
                return false;
            }
            finally
            {

            }

            return true;
        }

        public bool StoreOrderItemsToDB(List<OrderItem> orderItemList, string amazonOrderID)
        {
            //DBLayerAct dbLayer = new DBLayerAct();
           // SqlTransaction dbTransaction = DBConnection.GetNewTransaction();
            try
            {
                PINLogger.LogNow("Inside StoreOrderItemsToDB");
                DataTable dt = ConvertOrderItemListToDataTable(orderItemList, amazonOrderID);
                DBConnector.RecordsList = dt;
                DBConnector.InsertItemsData();
                
            }
            catch (Exception ex)
            {
                PINLogger.LogNow("Error in StoreOrdersToDB: " + ex.Message);
                //dbTransaction.Rollback();
                return false;
            }
            finally
            {

            }

            return true;
        }


        public string GetNextToken(IMWSResponse response)
        {
            string responseXml = Utility.GetResponseXML(response);
            XmlDocument orderListDoc = new XmlDocument();
            orderListDoc.LoadXml(responseXml);
            if (orderListDoc == null)
                return string.Empty;
            XmlNode nextTokenNode = orderListDoc.SelectSingleNode("//NextToken");
            if (nextTokenNode != null)
            {
                if (!string.IsNullOrEmpty(nextTokenNode.InnerText))
                    return nextTokenNode.InnerText;
            }
  
            return string.Empty;
        }

        public string CheckNextToken()
        {
            string orderNextToken = string.Empty;
            FileInfo fileInfo = new FileInfo(nextTokenFile);
            if (fileInfo.Exists)
            {
                StreamReader streamReader = new StreamReader(nextTokenFile);
                orderNextToken = streamReader.ReadToEnd();
                streamReader.Close();
            }

            return orderNextToken;
        }

        public void SaveNextToken(string responseNextToken)
        {
            StreamWriter streamWriter = new StreamWriter(nextTokenFile);
            streamWriter.Write(responseNextToken);
            streamWriter.Close();
        }
        public DataTable ConvertListToDataTable(List<Order> list)
        {
            // New table.
            DataTable table = new DataTable();

            //string str = String.Join(",", typeof(Order).GetProperties().Select(p => p.Name));

            //string[] str1 = str.Split(',');
            // Get max columns.

            //DataColumn OrderId = new DataColumn();
            //OrderId.DataType = System.Type.GetType("System.String");
            //OrderId.ColumnName = "OrderId";
            //OrderId.AutoIncrement = true;

            //table.Columns.Add(OrderId);
            //DataColumn[] keys = new DataColumn[1];
            //keys[0] = OrderId;
            //table.PrimaryKey = keys;

            DataColumn AmazonOrderId = new DataColumn();
            AmazonOrderId.DataType = System.Type.GetType("System.String");
            AmazonOrderId.ColumnName = "AmazonOrderId";

            table.Columns.Add(AmazonOrderId);
            DataColumn[] keys = new DataColumn[1];
            keys[0] = AmazonOrderId;
            table.PrimaryKey = keys;

            table.Columns.Add("OrderId");            
            table.Columns.Add("SellerOrderId");
            table.Columns.Add("PurchaseDate");
            table.Columns.Add("LastUpdateDate");
            table.Columns.Add("OrderStatus");
            table.Columns.Add("FulfillmentChannel");
            table.Columns.Add("SalesChannel");
            table.Columns.Add("ShipServiceLevel");
            table.Columns.Add("NumberOfItemsShipped");
            table.Columns.Add("NumberOfItemsUnshipped");
            table.Columns.Add("PaymentMethod");
            table.Columns.Add("MarketplaceId");
            table.Columns.Add("ShipmentServiceLevelCategory");
            table.Columns.Add("OrderType");
            table.Columns.Add("EarliestShipDate");
            table.Columns.Add("LatestShipDate");
            table.Columns.Add("IsBusinessOrder");           
            table.Columns.Add("IsPrime");
            table.Columns.Add("IsPremiumOrder");
            table.Columns.Add("IsReplacementOrder");
            table.Columns.Add("BuyerEmail");
            table.Columns.Add("BuyerName");
            table.Columns.Add("CurrencyCode");
            table.Columns.Add("OrderAmount");
            table.Columns.Add("CompanyLegalName");
            table.Columns.Add("TaxingRegion");
            table.Columns.Add("TaxClassifications");
            table.Columns.Add("Name");
            table.Columns.Add("AddressLine1");
            table.Columns.Add("AddressLine2");
            table.Columns.Add("AddressLine3");
            table.Columns.Add("City");
            table.Columns.Add("StateOrRegion");
            table.Columns.Add("Country");
            table.Columns.Add("District");
            table.Columns.Add("Billing_Address_Name");
            table.Columns.Add("Billing_Address_Line1");
            table.Columns.Add("Billing_Address_Line2");
            table.Columns.Add("Billing_Address_Line3");
            table.Columns.Add("Billing_Address_City");
            table.Columns.Add("Billing_Address_StateOrRegion");
            table.Columns.Add("Billing_Address_CountryCode");
            table.Columns.Add("Billing_Address_Country");
            table.Columns.Add("Billing_Address_District");
            table.Columns.Add("Tenant_ID");


            //table.Columns.Add("OrderAmount", System.Type.GetType("System.Double"));
            //foreach (string colname in str1)
            //{
            //    table.Columns.Add(colname);
            //}
            //foreach (var lst in list)
            //{
            //    string str = String.Join(",", typeof(Order).GetProperties().Select(p => p.Name));

            //    //PINLogger.LogNow(lst);                
            //}
            //foreach (var array in list[0])
            //{
            //    if (array.length > columns)
            //    {
            //        columns = array.length;
            //    }
            //}

            //// Add columns.
            //for (int i = 0; i < list.Count; i++)
            //{
            //    table.Columns.Add();
            //}
            int recordNumber = GetLastRecordNumber("PIN_Orders", "OrderId");
            string strvalue = string.Empty;
            // Add rows.
            foreach (var rowValue in list)
            {
                //rowValue.SellerOrderId = rowValue.IsSetSellerOrderId() ? rowValue.SellerOrderId : string.Empty;
                try
                {
                    recordNumber = recordNumber + 1;
                    DataRow dr = table.NewRow();

                    dr["OrderId"] = recordNumber;                    
                    dr["AmazonOrderId"] = rowValue.AmazonOrderId;
                    dr["SellerOrderId"] = rowValue.IsSetSellerOrderId() ? rowValue.SellerOrderId : string.Empty;
                    dr["PurchaseDate"] = rowValue.PurchaseDate;
                    dr["LastUpdateDate"] = rowValue.IsSetLastUpdateDate() ? rowValue.LastUpdateDate : DateTime.MinValue;
                    dr["OrderStatus"] = rowValue.IsSetOrderStatus() ? rowValue.OrderStatus : string.Empty;
                    dr["FulfillmentChannel"] = rowValue.IsSetFulfillmentChannel() ? rowValue.FulfillmentChannel : string.Empty;
                    dr["SalesChannel"] = rowValue.IsSetSalesChannel() ? rowValue.SalesChannel : string.Empty;
                    dr["ShipServiceLevel"] = rowValue.IsSetShipServiceLevel() ? rowValue.ShipServiceLevel : string.Empty;
                    dr["NumberOfItemsShipped"] = rowValue.IsSetNumberOfItemsShipped() ? rowValue.NumberOfItemsShipped : 0;
                    dr["NumberOfItemsUnshipped"] = rowValue.IsSetNumberOfItemsUnshipped() ? rowValue.NumberOfItemsUnshipped : 0;
                    dr["PaymentMethod"] = rowValue.IsSetPaymentMethod() ? rowValue.PaymentMethod : string.Empty;
                    dr["MarketplaceId"] = rowValue.IsSetMarketplaceId() ? rowValue.MarketplaceId : string.Empty;
                    dr["ShipmentServiceLevelCategory"] = rowValue.IsSetShipmentServiceLevelCategory() ? rowValue.ShipmentServiceLevelCategory : string.Empty;
                    dr["OrderType"] = rowValue.IsSetOrderType() ? rowValue.OrderType : string.Empty;
                    dr["EarliestShipDate"] = rowValue.IsSetEarliestShipDate() ? rowValue.EarliestShipDate : DateTime.MinValue;
                    dr["LatestShipDate"] = rowValue.IsSetLatestShipDate() ? rowValue.LatestShipDate : DateTime.MinValue;
                    dr["IsBusinessOrder"] = rowValue.IsBusinessOrder;
                    dr["IsPrime"] = rowValue.IsPrime;
                    dr["IsPremiumOrder"] = rowValue.IsPremiumOrder;
                    dr["IsReplacementOrder"] = rowValue.IsReplacementOrder;
                    dr["BuyerEmail"] = rowValue.IsSetBuyerEmail() ? rowValue.BuyerEmail : string.Empty;
                    dr["BuyerName"] = rowValue.IsSetBuyerName() ? rowValue.BuyerName : string.Empty;
                    dr["CurrencyCode"] = rowValue.IsSetOrderTotal() && rowValue.OrderTotal.IsSetCurrencyCode() ? rowValue.OrderTotal.CurrencyCode : string.Empty;
                    dr["OrderAmount"] = rowValue.IsSetOrderTotal() && rowValue.OrderTotal.IsSetAmount() ? rowValue.OrderTotal.Amount : string.Format("{0}",0.00);
                    dr["CompanyLegalName"] = rowValue.IsSetBuyerTaxInfo() && rowValue.BuyerTaxInfo.IsSetCompanyLegalName() ? rowValue.BuyerTaxInfo.CompanyLegalName : string.Empty;
                    dr["TaxingRegion"] = rowValue.IsSetBuyerTaxInfo() && rowValue.BuyerTaxInfo.IsSetTaxingRegion() ? rowValue.BuyerTaxInfo.TaxingRegion : string.Empty;
                    dr["TaxClassifications"] = string.Empty;
                    dr["Name"] = rowValue.ShippingAddress!=null && rowValue.ShippingAddress.IsSetName() ? rowValue.ShippingAddress.Name : string.Empty;
                    dr["AddressLine1"] = rowValue.ShippingAddress != null && rowValue.ShippingAddress.IsSetAddressLine1() ? rowValue.ShippingAddress.AddressLine1 : string.Empty;
                    dr["AddressLine2"] = rowValue.ShippingAddress != null && rowValue.ShippingAddress.IsSetAddressLine2() ? rowValue.ShippingAddress.AddressLine2 : string.Empty;
                    dr["AddressLine3"] = rowValue.ShippingAddress != null && rowValue.ShippingAddress.IsSetAddressLine3() ? rowValue.ShippingAddress.AddressLine3 : string.Empty;
                    dr["City"] = rowValue.ShippingAddress != null && rowValue.ShippingAddress.IsSetCity() ? rowValue.ShippingAddress.City : string.Empty;
                    dr["StateOrRegion"] = rowValue.ShippingAddress != null && rowValue.ShippingAddress.IsSetStateOrRegion() ? rowValue.ShippingAddress.StateOrRegion : string.Empty;
                    dr["Country"] = rowValue.ShippingAddress != null && rowValue.ShippingAddress.IsSetCounty() ? rowValue.ShippingAddress.County : string.Empty;
                    dr["District"] = rowValue.ShippingAddress != null && rowValue.ShippingAddress.IsSetDistrict() ? rowValue.ShippingAddress.District : string.Empty;
                    dr["Billing_Address_Name"]= string.Empty;
                    dr["Billing_Address_Line1"] = string.Empty;
                    dr["Billing_Address_Line2"] = string.Empty;
                    dr["Billing_Address_Line3"] = string.Empty;
                    dr["Billing_Address_City"] = string.Empty;
                    dr["Billing_Address_StateOrRegion"] = string.Empty;
                    dr["Billing_Address_CountryCode"] = string.Empty;
                    dr["Billing_Address_Country"] = string.Empty;
                    dr["Billing_Address_District"] = string.Empty;
                    dr["Tenant_ID"] = 1;


                    //dr["OrderAmount"] = rowValue.OrderTotal.Amount;
                    //dr["OrderAmount"] = String.Format("{0:C}", rowValue.OrderTotal);
                    table.Rows.Add(dr);
                    table.AcceptChanges();                  
                }
                catch (Exception ex)
                {
                    int i = recordNumber;
                    throw ex;
                }
                //break;
            }

            return table;
        }
        
        public DataTable ConvertOrderItemListToDataTable(List<OrderItem> orderItemlist, string platformOrderId)
        {
            // Order Item table
            DataTable table = new DataTable();
            DataColumn OrderItemId = new DataColumn();
            OrderItemId.DataType = System.Type.GetType("System.String");
            OrderItemId.ColumnName = "Order_Item_ID";

            table.Columns.Add(OrderItemId);
            DataColumn[] keys = new DataColumn[1];
            keys[0] = OrderItemId;
            table.PrimaryKey = keys;
            table.Columns.Add("PlatformOrder_Item_Id");
            table.Columns.Add("PlatformOrderID");
            table.Columns.Add("ASIN");
            table.Columns.Add("MERCHANTSKU");
            table.Columns.Add("OrderItemID");
            table.Columns.Add("Product_Title");
            table.Columns.Add("Quantity_Ordered");
            table.Columns.Add("Quantity_Shipped");
            table.Columns.Add("ItemPrice_Currency");
            table.Columns.Add("ItemPrice_Amount");
            table.Columns.Add("ItemTax_Currency");
            table.Columns.Add("ItemTax_Amount");
            table.Columns.Add("Promo_Currency");
            table.Columns.Add("Promo_Amount");
            table.Columns.Add("PromoIds");
            table.Columns.Add("Tenant_ID");
            int recordNumber = GetLastRecordNumber("PIN_OrderItems", "Order_Item_ID");
            string strvalue = string.Empty;
            // Add rows.
            foreach (var rowValue in orderItemlist)
            {
                //rowValue.SellerOrderId = rowValue.IsSetSellerOrderId() ? rowValue.SellerOrderId : string.Empty;
                try
                {
                    recordNumber = recordNumber + 1;
                    DataRow dr = table.NewRow();

                    dr["Order_Item_ID"] = recordNumber;
                    dr["PlatformOrder_Item_Id"] = rowValue.OrderItemId;
                    dr["PlatformOrderID"] = platformOrderId;
                    dr["ASIN"] = rowValue.ASIN;
                    dr["MERCHANTSKU"] = rowValue.SellerSKU;
                    dr["OrderItemID"] = rowValue.IsSetOrderItemId()? rowValue.OrderItemId : string.Empty;
                    dr["Product_Title"] = rowValue.IsSetTitle() ? rowValue.Title: string.Empty;
                    dr["Quantity_Ordered"] = rowValue.IsSetQuantityOrdered() ? rowValue.QuantityOrdered.ToString() : string.Empty;
                    dr["Quantity_Shipped"] = rowValue.IsSetQuantityShipped() ? rowValue.QuantityShipped.ToString() : string.Empty;
                    if (rowValue.ItemPrice != null)
                    {
                        dr["ItemPrice_Currency"] = rowValue.ItemPrice.IsSetCurrencyCode() ? rowValue.ItemPrice.CurrencyCode : string.Empty;
                        dr["ItemPrice_Amount"] = rowValue.ItemPrice.IsSetAmount() ? rowValue.ItemPrice.Amount : string.Empty;
                    }
                    else
                    {
                        dr["ItemPrice_Currency"] = string.Empty;
                        dr["ItemPrice_Amount"] = string.Empty;
                    }
                    if (rowValue.ItemTax != null)
                    {
                        dr["ItemTax_Currency"] = rowValue.ItemTax.IsSetCurrencyCode() ? rowValue.ItemTax.CurrencyCode : string.Empty;
                        dr["ItemTax_Amount"] = rowValue.ItemTax.IsSetAmount() ? rowValue.ItemTax.Amount : string.Empty;
                    }
                    else
                    {
                        dr["ItemTax_Currency"] = string.Empty;
                        dr["ItemTax_Amount"] = string.Empty;
                    }
                    if (rowValue.PromotionDiscount != null)
                    {
                        dr["Promo_Currency"] = rowValue.PromotionDiscount.IsSetCurrencyCode() ? rowValue.PromotionDiscount.CurrencyCode : string.Empty;
                        dr["Promo_Amount"] = rowValue.PromotionDiscount.IsSetAmount() ? rowValue.PromotionDiscount.Amount : string.Empty;
                    }
                    else
                    {
                        dr["Promo_Currency"] = string.Empty;
                        dr["Promo_Amount"] = string.Empty;
                    }
                    //dr["PromoIds"] = rowValue.PromotionIds.to ? rowValue.LatestShipDate : DateTime.MinValue;
                    dr["Tenant_ID"] = 1;
                    table.Rows.Add(dr);
                    table.AcceptChanges();
                }
                catch (Exception ex)
                {
                    int i = recordNumber;
                    throw ex;
                }
                //break;
            }

            return table;
        }

        public int GetLastRecordNumber(string strTable, string strfieldName)
        {    
            return DBOperations.GetLastRecordNumber(strTable, strfieldName);
        }
       
    }
}
