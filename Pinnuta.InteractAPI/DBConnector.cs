﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pinnuta.DBContext;
using Pinnuta.InteractAPI.Platform;
using Pinnuta.CommonComponents;

namespace Pinnuta.InteractAPI
{
    public static class DBConnector
    {       
        public static DataTable RecordsList
        {
            get { return DBOperations.RecordsList; }
            set { DBOperations.RecordsList = value; }
        }
        private static DataTable GetAuthenticationDetails()
        {
            return DBOperations.GetAuthenticationDetails();                      
        }
        public static void GetData()
        {
            IDataRequestProcess platformInbound;
            string nextTokenCallBack = string.Empty;
            IDictionary<string, Client> clientList = ClientHelper.GetClientList();

            foreach (Client client in clientList.Values)
            {
                do
                {
                    DataBatchResponse dataBatchResponse = null;
                    platformInbound = new InboundAmazon(client, callbackNextToken: nextTokenCallBack);
                    dataBatchResponse = platformInbound.ExecutePlatformInboundData();

                    nextTokenCallBack = dataBatchResponse.CallBackToken;
                }
                while (!string.IsNullOrEmpty(nextTokenCallBack));
            }
            //DataTable dt = GetAuthenticationDetails();
            //foreach (DataRow dr in dt.Rows)
            //{
            //    AuthenticationEntity.AccessKey = dr["Platform_Accesskey"].ToString();
            //    AuthenticationEntity.SecretKey = dr["Platfrom_Secretkey"].ToString();
            //    AuthenticationEntity.MarketID = dr["Platform_Market_ID"].ToString();
            //    AuthenticationEntity.SellerID = dr["Platfrom_Seller_ID"].ToString();
            //    AuthenticationEntity.AuthToken = dr["Platform_Auth_Token"].ToString();
            //    AuthenticationEntity.ServiceURL = dr["Platform_ServiceURL"].ToString();

            //    PullInfoHelper pullInfo = new PullInfoHelper();
            //    pullInfo.PullAllLastMonthOrdersToDB();
            //    pullInfo.PullAllOrderItemsToDB();
            //}

            //PullInfoHelperForProducts pullInfoProducts = new PullInfoHelperForProducts();
            //pullInfoProducts.PullAllLastMonthOrdersToDB();
        }
        public static void InsertData()
        {
            DBOperations.InsertData();
        }

        public static void InsertItemsData()
        {
            DBOperations.InsertItemsData();
        }
    }
}
