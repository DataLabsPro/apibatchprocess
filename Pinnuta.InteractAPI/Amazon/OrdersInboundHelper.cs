/*******************************************************************************
 * Copyright 2009-2017 Amazon Services. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 *
 * You may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *******************************************************************************
 * Marketplace Web Service Orders
 * API Version: 2013-09-01
 * Library Version: 2017-02-22
 * Generated: Thu Mar 02 12:41:05 UTC 2017
 */

using Pinnuta.PlatformCore;
using Pinnuta.PlatformCore.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using Pinnuta.DBContext;
using Pinnuta.CommonComponents.PlatformAuth;
using Pinnuta.Logging;
using Pinnuta.CommonComponents;

namespace Pinnuta.InteractAPI
{

    /// <summary>
    /// Runnable sample code to demonstrate usage of the C# client.
    ///
    /// To use, import the client source as a console application,
    /// and mark this class as the startup object. Then, replace
    /// parameters below with sensible values and run.
    /// </summary>
    public class OrdersInboundHelper
    {
        string ORDERDIR = "c:\\DIGITUP2\\Orders\\";
        //string SELLERID = "A2B0LQJYBA2OQJ";
        //string MWSAUTHTOKEN = "amzn.mws.26b06fb7-055b-845c-5eac-15e62bb00f5a";
        public AmazonAuthentication Authentication
        {
            get;
            set;
        }

        public MarketplaceWebServiceOrders client
        {
            get;
            set;
        }

        //public MarketplaceWebServiceOrders Client => client;

        public OrdersInboundHelper(AmazonAuthentication authentication)//MarketplaceWebServiceOrders client)
        {
            this.Authentication = authentication;
            // The client application name
            string appName = "Pinnuta";

            // The client application version
            string appVersion = "1.0";

            MarketplaceWebServiceOrdersConfig config = new MarketplaceWebServiceOrdersConfig();

            config.ServiceURL = this.Authentication.ServiceURL;

            //config.WithProxyHost("uscdtcpxy.internal.imsglobal.com");
            //config.WithProxyPort(8080);
            //config.WithProxyUsername("internal\\selangovan");
            //config.WithProxyPassword("myMercury!9");

            // Set other client connection configurations here if needed
            // Create the client itself
            MarketplaceWebServiceOrders client = new MarketplaceWebServiceOrdersClient(this.Authentication.AccessKey, this.Authentication.SecretKey, appName, appVersion, config);
            this.client = client;

            //set masked flag
            //if (DBLayer.DBLayerAct.IsMaskedFlag)
            //{
            //    //do nothing
            //}

        }

        public GetOrderResponse InvokeGetOrder()
        {
            // Create a request.
            GetOrderRequest request = new GetOrderRequest();
            request.SellerId = AuthenticationDetails.SellerID;
            request.MWSAuthToken = AuthenticationDetails.AuthToken;

            List<string> amazonOrderId = new List<string>();
            amazonOrderId.Add("102-9234878-9490628");
            request.AmazonOrderId = amazonOrderId;

            return this.client.GetOrder(request);
        }

        public GetServiceStatusResponse InvokeGetServiceStatus()
        {
            // Create a request.
            GetServiceStatusRequest request = new GetServiceStatusRequest();
            request.SellerId = AuthenticationDetails.SellerID;
            request.MWSAuthToken = AuthenticationDetails.AuthToken;
            return this.client.GetServiceStatus(request);
        }

        public ListOrderItemsResponse InvokeListOrderItems(string amzOrderID)
        {
            // Create a request.
            ListOrderItemsRequest request = new ListOrderItemsRequest();
            request.SellerId = this.Authentication.SellerID; //AuthenticationDetails.SellerID;
            request.MWSAuthToken = this.Authentication.AuthToken;
            request.AmazonOrderId = amzOrderID;
            return this.client.ListOrderItems(request);
        }

        public ListOrderItemsByNextTokenResponse InvokeListOrderItemsByNextToken(string nextToken)
        {
            // Create a request.
            ListOrderItemsByNextTokenRequest request = new ListOrderItemsByNextTokenRequest();
            request.SellerId = this.Authentication.SellerID;
            request.MWSAuthToken = this.Authentication.AuthToken;
            request.NextToken = nextToken;
            return this.client.ListOrderItemsByNextToken(request);
        }

        public ListOrdersResponse InvokeListOrders(bool fullYear = false)
        {
            // Create a request.
            ListOrdersRequest request = new ListOrdersRequest();
            request.SellerId = AuthenticationDetails.SellerID;
            request.MWSAuthToken = AuthenticationDetails.AuthToken;
            if (!fullYear)
            {
                DateTime createdAfter = DateTime.Now.AddDays(-1);
                request.CreatedAfter = createdAfter;
            }
            else
            {
                DateTime createdAfter = DateTime.Now.AddDays(-DateTime.Now.DayOfYear);
                request.CreatedAfter = createdAfter;
            }
            //DateTime createdBefore = DateTime.Now;
            //request.CreatedBefore = createdBefore;
            //DateTime lastUpdatedAfter = new DateTime();
            //request.LastUpdatedAfter = lastUpdatedAfter;
            //DateTime lastUpdatedBefore = new DateTime();
            //request.LastUpdatedBefore = lastUpdatedBefore;
            List<string> orderStatus = new List<string>();
            request.OrderStatus = orderStatus;
            List<string> marketplaceId = new List<string>();
            marketplaceId.Add(AuthenticationDetails.MarketID);
            request.MarketplaceId = marketplaceId;
            List<string> fulfillmentChannel = new List<string>();
            fulfillmentChannel.Add("AFN");
            fulfillmentChannel.Add("MFN");
            request.FulfillmentChannel = fulfillmentChannel;
            //List<string> paymentMethod = new List<string>();
            //request.PaymentMethod = paymentMethod;
            //string buyerEmail = "example";
            //request.BuyerEmail = buyerEmail;
            //string sellerOrderId = "example";
            //request.SellerOrderId = sellerOrderId;
            decimal maxResultsPerPage = 100;
            request.MaxResultsPerPage = maxResultsPerPage;
            //List<string> tfmShipmentStatus = new List<string>();
            //request.TFMShipmentStatus = tfmShipmentStatus;
            return this.client.ListOrders(request);
        }

        protected void GetOrdersPeriod(InboundProcessPeriod inboundProcessPeriod, out DateTime orderCreatedDate, out DateTime orderBeforeDate)
        {
            orderCreatedDate = DateTime.Now;
            orderBeforeDate = DateTime.Now;
            switch (inboundProcessPeriod)
            {
                case InboundProcessPeriod.PreviousDayOnly:
                    orderCreatedDate = DateTimeHelper.GetPreviousDayStartDateTime();
                    orderBeforeDate = DateTimeHelper.GetPreviousDayEndDateTime();
                    break;
                case InboundProcessPeriod.PreviousLast30Only:
                    //this is right now for last month
                    orderCreatedDate = DateTimeHelper.GetLastMonthStartDateTime();
                    orderBeforeDate = DateTimeHelper.GetLastMonthEndDateTime();
                    break;
                case InboundProcessPeriod.Previous3MonthOnly:
                    break;
                case InboundProcessPeriod.Previous6MonthOnly:
                    break;
                case InboundProcessPeriod.AllUp2Date:
                    break;
                default:
                    break;

            }
            return;
        }

        public ListOrdersResponse InvokeListOrders(InboundProcessPeriod inboundProcessPeriod)
        {
            // Create a request.
            ListOrdersRequest request = new ListOrdersRequest();
            request.SellerId = this.Authentication.SellerID;
            request.MWSAuthToken = this.Authentication.AuthToken;

            DateTime createdAfter;
            DateTime createdBefore;
            GetOrdersPeriod(InboundProcessPeriod.PreviousLast30Only, out createdAfter, out createdBefore );
            request.CreatedAfter = createdAfter;
            request.CreatedBefore = createdBefore;
            List<string> orderStatus = new List<string>();
            request.OrderStatus = orderStatus;
            List<string> marketplaceId = new List<string>();
            marketplaceId.Add(this.Authentication.MarketID);
            request.MarketplaceId = marketplaceId;
            List<string> fulfillmentChannel = new List<string>();
            fulfillmentChannel.Add("AFN");
            fulfillmentChannel.Add("MFN");
            request.FulfillmentChannel = fulfillmentChannel;
            //List<string> paymentMethod = new List<string>();
            //request.PaymentMethod = paymentMethod;
            //string buyerEmail = "example";
            //request.BuyerEmail = buyerEmail;
            //string sellerOrderId = "example";
            //request.SellerOrderId = sellerOrderId;
            decimal maxResultsPerPage = 100;
            request.MaxResultsPerPage = maxResultsPerPage;
            //List<string> tfmShipmentStatus = new List<string>();
            //request.TFMShipmentStatus = tfmShipmentStatus;
            return this.client.ListOrders(request);
        }

        public ListOrdersResponse InvokeListOrdersLast3Months()
        {
            // Create a request.
            ListOrdersRequest request = new ListOrdersRequest();
            request.SellerId = AuthenticationDetails.SellerID;
            request.MWSAuthToken = AuthenticationDetails.AuthToken;

           // request.CreatedAfter = Utility.GetAMZFormat3MonthStart();

            List<string> orderStatus = new List<string>();
            request.OrderStatus = orderStatus;
            List<string> marketplaceId = new List<string>();
            marketplaceId.Add(AuthenticationDetails.MarketID);
            request.MarketplaceId = marketplaceId;
            List<string> fulfillmentChannel = new List<string>();
            fulfillmentChannel.Add("AFN");
            fulfillmentChannel.Add("MFN");
            request.FulfillmentChannel = fulfillmentChannel;
            decimal maxResultsPerPage = 100;
            request.MaxResultsPerPage = maxResultsPerPage;
            return this.client.ListOrders(request);
        }

        /// <summary>
        /// pull last 6 month of order data to till date
        /// </summary>
        /// <returns></returns>
        public ListOrdersResponse InvokeListOrdersLast6Months()
        {
            // Create a request.
            ListOrdersRequest request = new ListOrdersRequest();
            request.SellerId = AuthenticationDetails.SellerID;
            request.MWSAuthToken = AuthenticationDetails.AuthToken;

            //request.CreatedAfter = Utility.GetAMZFormat6MonthStart();

            List<string> orderStatus = new List<string>();
            request.OrderStatus = orderStatus;
            List<string> marketplaceId = new List<string>();
            marketplaceId.Add(AuthenticationDetails.MarketID);
            request.MarketplaceId = marketplaceId;
            List<string> fulfillmentChannel = new List<string>();
            fulfillmentChannel.Add("AFN");
            fulfillmentChannel.Add("MFN");
            request.FulfillmentChannel = fulfillmentChannel;
            decimal maxResultsPerPage = 100;
            request.MaxResultsPerPage = maxResultsPerPage;
            return this.client.ListOrders(request);
        }

        /// <summary>
        /// list of 3 years of data
        /// </summary>
        /// <returns></returns>
        public ListOrdersResponse InvokeListOrdersALL()
        {
            // Create a request.
            ListOrdersRequest request = new ListOrdersRequest();
            request.SellerId = AuthenticationDetails.SellerID;
            request.MWSAuthToken = AuthenticationDetails.AuthToken;
          //  request.CreatedAfter = Utility.GetAMZFormatAllOrders();
            List<string> orderStatus = new List<string>();
            request.OrderStatus = orderStatus;
            List<string> marketplaceId = new List<string>();
            marketplaceId.Add(AuthenticationDetails.MarketID);
            request.MarketplaceId = marketplaceId;
            List<string> fulfillmentChannel = new List<string>();
            fulfillmentChannel.Add("AFN");
            fulfillmentChannel.Add("MFN");
            request.FulfillmentChannel = fulfillmentChannel;
            decimal maxResultsPerPage = 100;
            request.MaxResultsPerPage = maxResultsPerPage;
            return this.client.ListOrders(request);
        }


        public ListOrdersResponse InvokeListOrdersCurrentMonth()
        {
            // Create a request.
            ListOrdersRequest request = new ListOrdersRequest();
            request.SellerId = AuthenticationDetails.SellerID;
            request.MWSAuthToken = AuthenticationDetails.AuthToken;

           // DateTime createdAfter = Utility.GetAMZFormatCurrentMonthStart();
            //request.CreatedAfter = createdAfter; //createdAfter.AddMinutes(-5);

            //DateTime createdBefore = createdAfter.AddMonths(1);
          //  DateTime createdBefore = Utility.GetAMZFormatCurrentDate();
           // request.CreatedBefore = createdAfter.AddMonths(1).AddMinutes(-5);
            //DateTime lastUpdatedAfter = new DateTime();
            //request.LastUpdatedAfter = lastUpdatedAfter;
            //DateTime lastUpdatedBefore = new DateTime();
            //request.LastUpdatedBefore = lastUpdatedBefore;
            List<string> orderStatus = new List<string>();
            request.OrderStatus = orderStatus;
            List<string> marketplaceId = new List<string>();
            marketplaceId.Add(AuthenticationDetails.MarketID);
            request.MarketplaceId = marketplaceId;
            List<string> fulfillmentChannel = new List<string>();
            fulfillmentChannel.Add("AFN");
            fulfillmentChannel.Add("MFN");
            request.FulfillmentChannel = fulfillmentChannel;
            //List<string> paymentMethod = new List<string>();
            //request.PaymentMethod = paymentMethod;
            //string buyerEmail = "example";
            //request.BuyerEmail = buyerEmail;
            //string sellerOrderId = "example";
            //request.SellerOrderId = sellerOrderId;
            decimal maxResultsPerPage = 100;
            request.MaxResultsPerPage = maxResultsPerPage;
            //List<string> tfmShipmentStatus = new List<string>();
            //request.TFMShipmentStatus = tfmShipmentStatus;
            return this.client.ListOrders(request);
        }

        public ListOrdersResponse InvokeListOrdersLastMonthForDay(int day)
        {
            // Create a request.
            ListOrdersRequest request = new ListOrdersRequest();
            request.SellerId = AuthenticationDetails.SellerID;
            request.MWSAuthToken = AuthenticationDetails.AuthToken;

            DateTime createdAfter = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, day);
            request.CreatedAfter = createdAfter;//.AddDays(-1);

            DateTime createdBefore = createdAfter.AddDays(1);
            //DateTime createdBefore = DateTime.Now;
            request.CreatedBefore = createdBefore;
            //DateTime lastUpdatedAfter = new DateTime();
            //request.LastUpdatedAfter = lastUpdatedAfter;
            //DateTime lastUpdatedBefore = new DateTime();
            //request.LastUpdatedBefore = lastUpdatedBefore;
            List<string> orderStatus = new List<string>();
            request.OrderStatus = orderStatus;
            List<string> marketplaceId = new List<string>();
            marketplaceId.Add(AuthenticationDetails.MarketID);
            request.MarketplaceId = marketplaceId;
            List<string> fulfillmentChannel = new List<string>();
            fulfillmentChannel.Add("AFN");
            fulfillmentChannel.Add("MFN");
            request.FulfillmentChannel = fulfillmentChannel;
            //List<string> paymentMethod = new List<string>();
            //request.PaymentMethod = paymentMethod;
            //string buyerEmail = "example";
            //request.BuyerEmail = buyerEmail;
            //string sellerOrderId = "example";
            //request.SellerOrderId = sellerOrderId;
            decimal maxResultsPerPage = 100;
            request.MaxResultsPerPage = maxResultsPerPage;
            //List<string> tfmShipmentStatus = new List<string>();
            //request.TFMShipmentStatus = tfmShipmentStatus;
            return this.client.ListOrders(request);
        }

        public ListOrdersByNextTokenResponse InvokeListOrdersByNextToken(string nextToken)
        {
            // Create a request.
            ListOrdersByNextTokenRequest request = new ListOrdersByNextTokenRequest();
            request.SellerId = this.Authentication.SellerID; //AuthenticationDetails.SellerID;
            request.MWSAuthToken = this.Authentication.AuthToken; //AuthenticationDetails.AuthToken;
            request.NextToken = nextToken;
            return this.client.ListOrdersByNextToken(request);
        }


    }
}
