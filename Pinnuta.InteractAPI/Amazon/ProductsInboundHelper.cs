/*******************************************************************************
 * Copyright 2009-2017 Amazon Services. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 *
 * You may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *******************************************************************************
 * Marketplace Web Service Orders
 * API Version: 2013-09-01
 * Library Version: 2017-02-22
 * Generated: Thu Mar 02 12:41:05 UTC 2017
 */
using Pinnuta.PlatformCore.Products;
using Pinnuta.PlatformCore.Products.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using Pinnuta.DBContext;


namespace Pinnuta.InteractAPI
{

    /// <summary>
    /// Runnable sample code to demonstrate usage of the C# client.
    ///
    /// To use, import the client source as a console application,
    /// and mark this class as the startup object. Then, replace
    /// parameters below with sensible values and run.
    /// </summary>
    public class ProductsInboundHelper
    {
        string ORDERDIR = "c:\\DIGITUP2\\Orders\\";
       
        public void PullDailyProducts()
        {
            // TODO: Set the below configuration variables before attempting to run

            
            //this.Client = client;
            //MarketplaceWebServiceOrdersSample sample = new MarketplaceWebServiceOrdersSample(client);

            // Uncomment the operation you'd like to test here
            // TODO: Modify the request created in the Invoke method to be valid

            try
            {
                IMWSResponse response = null;

                //Order/AmazonOrderId
                ////  response = sample.InvokeGetOrder();
                //  response = sample.InvokeGetServiceStatus();
                ////  response = sample.InvokeListOrderItems();
                // response = sample.InvokeListOrderItemsByNextToken();
                response = InvokeGetMatchingProduct();
                                // response = sample.InvokeListOrdersByNextToken();
                //PINLogger.LogNow("Response:");
                //ResponseHeaderMetadata rhmd = response.ResponseHeaderMetadata;
                //// We recommend logging the request id and timestamp of every call.
                //PINLogger.LogNow("RequestId: " + rhmd.RequestId);
                //PINLogger.LogNow("Timestamp: " + rhmd.Timestamp);
                //string responseXml = Utility.GetResponseXML(response);

                //XmlDocument orderListDoc = new XmlDocument();
                //orderListDoc.LoadXml(responseXml);
                //XmlNode root = orderListDoc.DocumentElement;
                //XmlNode nextTokenNodet = root.SelectSingleNode("//CreatedBefore");
                //string tt = nextTokenNodet.InnerText;
                //string filename = ORDERDIR + "ORDERS_" + DateTime.Now.AddDays(-1).ToShortDateString().Replace('/','_') + ".xml";
                //string yearFileName = ORDERDIR + "ORDERS_" + DateTime.Now.Year +".xml";
                //FileInfo fInfo = new FileInfo(yearFileName);
                
                //orderListDoc.Save(filename);
                //if (!fInfo.Exists)
                //{
                //    ListMatchingProductsResponse orderResponse = InvokeListOrders(true);
                //    List<Order> orderList = orderResponse.ListOrdersResult.Orders;
                //    foreach (Order order in orderList) //first list of order
                //    {
                //    }
                //    responseXml = Utility.GetResponseXML(response);
                //    orderListDoc.LoadXml(responseXml);
                //    XmlNodeList nextTokenNode = orderListDoc.SelectNodes("//NextToken");
                //    foreach (XmlNode node in nextTokenNode)
                //    {
                //        string st = node.InnerText;
                //    }
                //    orderListDoc.Save( yearFileName);
                //}
                //XmlNodeList nodeList = orderListDoc.SelectNodes("//Order/AmazonOrderId");
                //IList<string> orderIDs = new List<string>();
                //foreach (XmlNode node in nodeList)
                //{
                //    if (!orderIDs.Contains(node.InnerText))
                //        orderIDs.Add(node.InnerText);

                //    response = InvokeListOrderItems(node.InnerText);
                //    responseXml = Utility.GetResponseXML(response);
                //    orderListDoc.LoadXml(responseXml);
                //    XmlNodeList orderItemNode = orderListDoc.SelectNodes("//Order");
                //    string st = orderItemNode.Count.ToString();
                //}

                //PINLogger.LogNow(responseXml);
               // Console.ReadLine();
            }
            catch (Exception ex)
            {
                // Exception properties are important for diagnostics.
                //ResponseHeaderMetadata rhmd = ex.ResponseHeaderMetadata;
                //PINLogger.LogNow("Service Exception:");
                //if (rhmd != null)
                //{
                //    PINLogger.LogNow("RequestId: " + rhmd.RequestId);
                //    PINLogger.LogNow("Timestamp: " + rhmd.Timestamp);
                //}
                //PINLogger.LogNow("Message: " + ex.Message);
                //PINLogger.LogNow("StatusCode: " + ex.StatusCode);
                //PINLogger.LogNow("ErrorCode: " + ex.ErrorCode);
                //PINLogger.LogNow("ErrorType: " + ex.ErrorType);
                //Console.ReadLine();
                throw ex;
            }
        }

        public MarketplaceWebServiceProducts client
        {
            get;
            set;
        }

       

        //public MarketplaceWebServiceOrders Client => client;

        public ProductsInboundHelper()//MarketplaceWebServiceOrders client)
        {
            
            // The client application name
            string appName = "Pinnuta";

            // The client application version
            string appVersion = "1.0";

            MarketplaceWebServiceProductsConfig config = new MarketplaceWebServiceProductsConfig();

            config.ServiceURL = AuthenticationDetails.ServiceURL;

            //config.WithProxyHost("uscdtcpxy.internal.imsglobal.com");
            //config.WithProxyPort(8080);
            //config.WithProxyUsername("internal\\selangovan");
            //config.WithProxyPassword("myMercury!9");

            // Set other client connection configurations here if needed
            // Create the client itself
            MarketplaceWebServiceProducts client = new MarketplaceWebServiceProductsClient(appName, appVersion, AuthenticationDetails.AccessKey, AuthenticationDetails.SecretKey, config);            
            this.client = client;

            //set masked flag
            //if (DBLayer.DBLayerAct.IsMaskedFlag)
            //{
            //    //do nothing
            //}

        }
      
        public GetMatchingProductResponse InvokeGetMatchingProduct()
        {
            // Create a request.
            GetMatchingProductRequest request = new GetMatchingProductRequest();           
            request.SellerId = AuthenticationDetails.SellerID;           
            request.MWSAuthToken = AuthenticationDetails.AuthToken;
            string marketplaceId = "ATVPDKIKX0DER";
            request.MarketplaceId = marketplaceId;
            ASINListType asinList = new ASINListType();
            request.ASINList = asinList;
            return this.client.GetMatchingProduct(request);
        }


        public ListMatchingProductsResponse InvokeListMatchingProducts()
        {
            // Create a request.
            ListMatchingProductsRequest request = new ListMatchingProductsRequest();
            request.SellerId = AuthenticationDetails.SellerID;
            request.MWSAuthToken = AuthenticationDetails.AuthToken;
            request.MarketplaceId = AuthenticationDetails.MarketID;
            //request.Query = "0439708184";
            //request.Query = "B01D93Z89W";
            request.Query = "B06WLH4TP9";
            request.QueryContextId = "All";            
            return this.client.ListMatchingProducts(request);
        }



    }
}
