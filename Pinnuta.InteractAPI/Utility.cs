﻿using Pinnuta.PlatformCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Pinnuta.InteractAPI
{
    public static class Utility
    {
        public static string GetResponseXML(IMWSResponse response)
        {
            string responseXml = response.ToXML();
            string filter = @"xmlns(:\w+)?=""([^""]+)""|xsi(:\w+)?=""([^""]+)""";
            responseXml = Regex.Replace(responseXml, filter, "");
            return responseXml;
        }

        public static DateTime GetAMZFormatCurrentDate()
        {
            return DateTime.Now.ToUniversalTime();//.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffK");
        }

        public static DateTime GetAMZFormatCurrentMonthStart()
        {
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0).ToUniversalTime();//.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffK");
        }

        public static DateTime GetAMZFormat6MonthStart()
        {
            DateTime currentDate = DateTime.Now.AddMonths(-6);
            return new DateTime(currentDate.Year, currentDate.Month, 1, 0, 0, 0).ToUniversalTime();
        }

        public static DateTime GetAMZFormat3MonthStart()
        {
            DateTime currentDate = DateTime.Now.AddMonths(-3);
            return new DateTime(currentDate.Year, currentDate.Month, 1, 0, 0, 0).ToUniversalTime();
        }

        public static DateTime GetAMZFormatAllOrders()
        {
            DateTime currentDate = DateTime.Now.AddYears(-3);
            return new DateTime(currentDate.Year, currentDate.Month, 1, 0, 0, 0).ToUniversalTime();
        }
    }
}
