﻿using DBContext.DBHelper;
using Pinnuta.CommonComponents.PlatformAuth;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinnuta.InteractAPI.AuthenticationHelper
{
    public class AuthenticationHelper
    {

        public static AmazonAuthentication GetAmazonAuthentication(string tenantID)
        {
            DataTable authTable = AuthenticationDBHelper.GetUserPlatformAuthenticationDetails(tenantID);
            if (authTable.Rows.Count > 0)
            {
                AmazonAuthentication amazonAuth = new AmazonAuthentication();
                DataRow userPlatformRow = authTable.Rows[0];

                amazonAuth.AccessKey = userPlatformRow["Platform_Accesskey"].ToString();
                amazonAuth.SecretKey = userPlatformRow["Platfrom_Secretkey"].ToString();
                amazonAuth.MarketID = userPlatformRow["Platform_Market_ID"].ToString();
                amazonAuth.SellerID = userPlatformRow["Platfrom_Seller_ID"].ToString();
                amazonAuth.AuthToken = userPlatformRow["Platform_Auth_Token"].ToString();
                amazonAuth.ServiceURL = userPlatformRow["Platform_ServiceURL"].ToString();
                return amazonAuth;
            }

            return null;
        }

        //public static AmazonAuthentication GetCurentUserTenant()
        //{
        //    DataTable authTable = AuthenticationDBHelper.GetUserTenantID();
        //    if (authTable.Rows.Count > 0)
        //    {
        //        AmazonAuthentication amazonAuth = new AmazonAuthentication();
        //        DataRow userPlatformRow = authTable.Rows[0];

        //        amazonAuth.AccessKey = userPlatformRow["Platform_Accesskey"].ToString();
        //        amazonAuth.SecretKey = userPlatformRow["Platfrom_Secretkey"].ToString();
        //        amazonAuth.MarketID = userPlatformRow["Platform_Market_ID"].ToString();
        //        amazonAuth.SellerID = userPlatformRow["Platfrom_Seller_ID"].ToString();
        //        amazonAuth.AuthToken = userPlatformRow["Platform_Auth_Token"].ToString();
        //        amazonAuth.ServiceURL = userPlatformRow["Platform_ServiceURL"].ToString();
        //        return amazonAuth;
        //    }

        //    return null;
        //}

    }
}
